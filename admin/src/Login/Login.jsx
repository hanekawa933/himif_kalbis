import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { login } from "../actions/auth";
import "./Login.css";

import Alert from "../components/Alert";

const Login = ({ login, isAuthenticated, loading }) => {
  const [formData, setFormData] = useState({
    username: "",
    password: "",
  });

  const { username, password } = formData;

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async (e) => {
    e.preventDefault();
    login(username, password);
  };

  if (isAuthenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <div className="login">
      <div className="login__heading">Admin Login</div>
      <form className="login__form" onSubmit={(e) => onSubmit(e)}>
        <Alert />
        <div className="form-group">
          <label htmlFor="Username">Username</label>
          <input
            type="text"
            name="username"
            className="form-control"
            onChange={(e) => onChange(e)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="Password">Password</label>
          <input
            type="password"
            name="password"
            className="form-control"
            onChange={(e) => onChange(e)}
          />
        </div>
        <div className="form-group">
          <button className="btn btn-primary btn-block">Login</button>
        </div>
      </form>
    </div>
  );
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { login })(Login);
