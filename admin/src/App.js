import React, { useEffect } from "react";
import "./App.css";

import {
  // ADMIN
  Login,
  Dashboard,
  CreateAnggota,
  CreateEvent,
  CreateDokumen,
  ChangeUsername,
  ChangePassword,
  ViewAnggota,
  ViewDokumen,
  ViewEvent,
  ViewPengunjung,
  UpdateAnggota,
  UpdateDokumen,
  UpdateEvent,
  Pesan,

  //CLIENT
  Landing,
  Event,
  EventDetail,
  BeliEvent,
  Anggota,
  Dokumen,
  Footer,
  ValidasiEvent,
  Kontak,
} from "./componentPath";

//Redux
import { Provider, Store, loadUser, setAuthToken } from "./redux";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

if (localStorage.token) {
  setAuthToken(localStorage.token);
}

function App() {
  useEffect(() => {
    Store.dispatch(loadUser());
  }, []);
  return (
    <Provider store={Store}>
      <Router>
        <Switch>
          <div className="App">
            {/* CLIENT */}
            <Route exact path="/" component={Landing} />
            <Route exact path="/event" component={Event} />
            <Route exact path="/event/:nama_event" component={EventDetail} />
            <Route path="/event/beli/:nama_event" component={BeliEvent} />
            <Route path="/anggota" component={Anggota} />
            <Route exact path="/dokumen" component={Dokumen} />
            <Route exact path="/contact" component={Kontak} />
            <Route
              path="/event/verifikasi/:email&event:nama_event"
              component={ValidasiEvent}
            />

            {/* ADMIN */}
            <Route exact path="/admin" component={Login} />
            <Route exact path="/dashboard" component={Dashboard} />
            <Route path="/dashboard/create/anggota" component={CreateAnggota} />
            <Route path="/dashboard/create/event" component={CreateEvent} />
            <Route path="/dashboard/create/dokumen" component={CreateDokumen} />
            <Route path="/dashboard/view/pesan" component={Pesan} />
            <Route
              path="/dashboard/change/username/:nim"
              component={ChangeUsername}
            />

            <Route
              path="/dashboard/change/password/:nim"
              component={ChangePassword}
            />
            <Route path="/dashboard/view/anggota" component={ViewAnggota} />
            <Route path="/dashboard/view/dokumen" component={ViewDokumen} />
            <Route exact path="/dashboard/view/event" component={ViewEvent} />
            <Route
              path="/dashboard/view/event/:event_name"
              component={ViewPengunjung}
            />
            <Route
              exact
              path="/dashboard/update/event/:nama_event"
              component={UpdateEvent}
            />
            <Route
              exact
              path="/dashboard/update/dokumen/:nama_dokumen"
              component={UpdateDokumen}
            />
            <Route
              exact
              path="/dashboard/update/anggota/:nim"
              component={UpdateAnggota}
            />
          </div>
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
