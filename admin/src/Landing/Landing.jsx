import React, { useState, useEffect } from "react";
import NavbarClient from "../components/NavbarClient";
import { connect } from "react-redux";
import Alert from "../components/Alert";
import { sendMessage } from "../actions/create";
import { getEvent } from "../actions/view";
import "./Landing.css";
import { Link } from "react-router-dom";

const Landing = ({ sendMessage, getEvent, event }) => {
  useEffect(() => {
    getEvent();
  }, [getEvent]);
  const [formData, setFormData] = useState({
    nama: "",
    email: "",
    pesan: "",
  });

  const getLatestEvent = event[event.length - 1];
  const getLatestEventName = getLatestEvent && getLatestEvent.nama;
  const getLatestEventPoster = getLatestEvent && getLatestEvent.poster;

  const { nama, email, pesan } = formData;

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async (e) => {
    e.preventDefault();
    sendMessage({
      nama,
      email,
      pesan,
    });
    setFormData({
      nama: "",
      email: "",
      pesan: "",
    });
  };

  const eventDisplay =
    event.length === 0 ? (
      <>
        <div className="landingEvent__noData">
          <div className="display-4">Tidak Ada Event Yang Bisa Ditampilkan</div>
        </div>
      </>
    ) : (
      <>
        <Link
          to={`/event/${getLatestEventName}`}
          className="nav-link text-dark"
        >
          <div className="event__latest">
            Latest Event <i className="fas fa-angle-double-right"></i>
          </div>
        </Link>
        <Link
          to={`/event/${getLatestEventName}`}
          className="nav-link p-0 m-0 text-dark link-hover"
        >
          <img src={getLatestEventPoster} alt="Event" className="image" />
          <div className="middle">
            <Link
              to={`/event/${getLatestEventName}`}
              className="btn btn-dark btn-size d-flex justify-content-center align-items-center"
            >
              View Event
            </Link>
          </div>
        </Link>
      </>
    );

  const link = (
    <a href="#kontak" className="navbar__link">
      Contact
    </a>
  );

  return (
    <div>
      <NavbarClient link={link} />
      <div className="landing">
        <div className="landing__text text-light">
          <div className="landing__heading">INFINITY</div>
          <div className="landing__slogan">Solidarity For Integrity</div>
          <a href="#about" className="btn landing__button">
            View More
          </a>
        </div>
      </div>

      <div className="landing__about" id="about">
        <div className="about__text">
          <div className="about__title display-4 text-center">Tentang Kami</div>
          <div className="about__content">
            <p>
              HIMIF Kalbis adalah Himpunan Mahasiswa Jurusan Informatika di
              Kalbis Institute yang telah ada sejak Kalbis Institute didirikan.
              Tujuan dari pembuatan website ini adalah untuk mempermudah akses
              pendaftaran event, dokumen, dan info-info terkait anggota HMJ
              Informatika Kalbis Institute
            </p>
          </div>
        </div>
      </div>

      <div className="landing__event">{eventDisplay}</div>

      <div className="landing__contact" id="kontak">
        <div className="row p-0 m-0">
          <div className="col-lg-6 col-md-12 col-sm-12">
            <div className="contact__text">
              <div className="h3">Kontak Kami</div>
              <div className="contact__description">
                Ingin berkontakan dengan kami secara langsung? Kalian bisa
                kontak kami melalui
              </div>
              <div className="contact__list">
                <div className="contact__content">
                  <i
                    className="fab fa-instagram contact__icon"
                    style={{ color: "red", opacity: "0.8" }}
                  ></i>
                  <span className="contact__span">@himif.kalbis</span>
                </div>
                <div className="contact__content">
                  <i
                    className="fab fa-line contact__icon"
                    style={{ color: "green" }}
                  ></i>
                  <span className="contact__span">@kzg7928p</span>
                </div>
                Atau kalian bisa mengirim pesan melalui form berikut..
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-md-12 col-sm-12">
            <div className="contact__form">
              <div className="h3">Kirim Pesan</div>
              <form
                action=""
                className="form__contact"
                onSubmit={(e) => onSubmit(e)}
              >
                <Alert />
                <div className="form-group">
                  <label htmlFor="Nama Lengkap">Nama Lengkap</label>
                  <input
                    type="text"
                    className="form-control"
                    name="nama"
                    onChange={(e) => onChange(e)}
                    value={nama}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="Email">Email</label>
                  <input
                    type="text"
                    className="form-control"
                    name="email"
                    onChange={(e) => onChange(e)}
                    value={email}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="Pesan">Pesan</label>
                  <textarea
                    type="text"
                    className="form-control"
                    name="pesan"
                    onChange={(e) => onChange(e)}
                    rows="5"
                    value={pesan}
                  />
                </div>
                <button className="btn btn-primary btn-block">
                  Kirim Pesan
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  event: state.view.event,
  data: state.create.data,
  success: (state.create.success = false),
});

export default connect(mapStateToProps, { sendMessage, getEvent })(Landing);
