import React from "react";
import { Link, Redirect } from "react-router-dom";
import { logout } from "../../actions/auth";
import Logo from "../../assets/images/logo.png";
import "./Navbar.css";
import { connect } from "react-redux";
import {
  adminNavbar,
  anggotaNavbar,
  eventNavbar,
  dokumenNavbar,
} from "./navbarSettings";

const Navbar = ({ logout, isAuthenticated, user }) => {
  if (!isAuthenticated) {
    return <Redirect to="/" />;
  }

  const { anggotum } = user;

  return (
    <nav className="navbar navbar-expand-lg navbar-dark navbar__flex w-100 navbar__background visibility">
      <Link
        className="navbar-brand w-25 d-flex justify-content-center align-items-center"
        to="/"
      >
        <img src={Logo} alt="Infinity Logo" className="navbar__logo" />
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto w-100 navbar__custom d-flex justify-content-around">
          <li className={`nav-item nav-item-custom`}>
            <Link className="navbar__link" to="/dashboard">
              Home
            </Link>
          </li>
          {anggotum && anggotum.divisi.divisi === "Anggota"
            ? anggotaNavbar
            : anggotum && anggotum.divisi.divisi === "Dokumen"
            ? dokumenNavbar
            : anggotum && anggotum.divisi.divisi === "Event"
            ? eventNavbar
            : adminNavbar}
          <li className="nav-item nav-item-custom">
            <a
              className="navbar__link dropdown-toggle"
              href="#!"
              id="navbarDropdown"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              Profile
            </a>
            <div
              className="dropdown-menu dropdown__custom"
              aria-labelledby="navbarDropdown"
            >
              <Link
                className="dropdown-item"
                to={`/dashboard/change/username/${user.username}`}
              >
                Ganti Username
              </Link>
              <Link
                className="dropdown-item"
                to={`/dashboard/change/password/${user.username}`}
              >
                Ganti Password
              </Link>
            </div>
          </li>
          <li className="nav-item nav-item-custom">
            <Link className="navbar__link" to="/" onClick={logout}>
              Logout
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
  user: state.auth.user,
});

export default connect(mapStateToProps, { logout })(Navbar);
