import React from "react";
import { Link } from "react-router-dom";

export const adminNavbar = (
  <>
    <li className="nav-item nav-item-custom">
      <a
        className="navbar__link dropdown-toggle"
        href="#!"
        id="navbarDropdown"
        role="button"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
      >
        Anggota
      </a>
      <div
        className="dropdown-menu dropdown__custom"
        aria-labelledby="navbarDropdown"
      >
        <Link className="dropdown-item" to="/dashboard/view/anggota">
          Lihat Anggota
        </Link>
        <Link className="dropdown-item" to="/dashboard/create/anggota">
          Buat Anggota
        </Link>
      </div>
    </li>
    <li className="nav-item nav-item-custom">
      <a
        className="navbar__link dropdown-toggle"
        href="#!"
        id="navbarDropdown"
        role="button"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
      >
        Event
      </a>
      <div
        className="dropdown-menu dropdown__custom dropdown__custom"
        aria-labelledby="navbarDropdown"
      >
        <Link className="dropdown-item" to="/dashboard/view/event">
          Lihat Event
        </Link>
        <Link className="dropdown-item" to="/dashboard/create/event">
          Buat Event
        </Link>
      </div>
    </li>
    <li className="nav-item nav-item-custom">
      <a
        className="navbar__link dropdown-toggle"
        href="#!"
        id="navbarDropdown"
        role="button"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
      >
        Dokumen
      </a>
      <div
        className="dropdown-menu dropdown__custom"
        aria-labelledby="navbarDropdown"
      >
        <Link className="dropdown-item" to="/dashboard/view/dokumen">
          Lihat Dokumen
        </Link>
        <Link className="dropdown-item" to="/dashboard/create/dokumen">
          Buat Dokumen
        </Link>
      </div>
    </li>
  </>
);

export const anggotaNavbar = (
  <>
    <li className="nav-item nav-item-custom">
      <a
        className="navbar__link dropdown-toggle"
        href="#!"
        id="navbarDropdown"
        role="button"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
      >
        Anggota
      </a>
      <div
        className="dropdown-menu dropdown__custom"
        aria-labelledby="navbarDropdown"
      >
        <Link className="dropdown-item" to="/dashboard/view/anggota">
          Lihat Anggota
        </Link>
        <Link className="dropdown-item" to="/dashboard/create/anggota">
          Buat Anggota
        </Link>
      </div>
    </li>
  </>
);

export const eventNavbar = (
  <li className="nav-item nav-item-custom">
    <a
      className="navbar__link dropdown-toggle"
      href="#!"
      id="navbarDropdown"
      role="button"
      data-toggle="dropdown"
      aria-haspopup="true"
      aria-expanded="false"
    >
      Event
    </a>
    <div
      className="dropdown-menu dropdown__custom"
      aria-labelledby="navbarDropdown"
    >
      <Link className="dropdown-item" to="/dashboard/view/event">
        Lihat Event
      </Link>
      <Link className="dropdown-item" to="/dashboard/create/event">
        Buat Event
      </Link>
    </div>
  </li>
);

export const dokumenNavbar = (
  <>
    <li className="nav-item nav-item-custom">
      <a
        className="navbar__link dropdown-toggle"
        href="#!"
        id="navbarDropdown"
        role="button"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
      >
        Dokumen
      </a>
      <div
        className="dropdown-menu dropdown__custom"
        aria-labelledby="navbarDropdown"
      >
        <Link className="dropdown-item" to="/dashboard/view/dokumen">
          Lihat Dokumen
        </Link>
        <Link className="dropdown-item" to="/dashboard/create/dokumen">
          Buat Dokumen
        </Link>
      </div>
    </li>
  </>
);
