import React from "react";
import "./CardDokumen.css";

const CardDokumen = (props) => {
  return (
    <div class="card text-center mt-5">
      <i className={`${props.icon}`}></i>
      <div class="card-body">
        <h5 class="card-title">{props.nama}</h5>
        <div className="d-flex flex-column mt-5 justify-content-center align-items-center">
          <a
            href={props.file}
            class="btn btn-primary mt-2 cardDokumen__button"
            download
          >
            Download Dokumen
          </a>
        </div>
      </div>
    </div>
  );
};

export default CardDokumen;
