import React from "react";
import { Link } from "react-router-dom";
import "./CardEvent.css";

const CardEvent = (props) => {
  const setDate = new Date(props.tanggal);
  const month = setDate.toLocaleString("default", { month: "short" });
  const year = setDate.getFullYear();
  const date = setDate.getDay();
  const formatDate = `${(`0` + date).slice(-2)}/${month}/${year}`;
  return (
    <div className="card font-weight-bold mt-5">
      <img
        className="card-img-top card-img-width"
        src={props.poster}
        alt="Event"
      />
      <div className="card-body">
        <h5 className="card-title">{props.nama}</h5>
        <div className="row">
          <div className="col-lg-4 col-md-4 col-sm-12">
            <p className="card-text">
              <i className="fas fa-map-marker-alt"></i> {props.lokasi}
            </p>
          </div>
          <div className="col-lg-4 col-md-4 col-sm-12">
            <p className="card-text">
              <i className="fas fa-calendar-week"></i> {formatDate}
            </p>
          </div>
          <div className="col-lg-4 col-md-4 col-sm-12">
            <p className="card-text">
              <i className="fas fa-clock"></i> {props.mulai}-{props.berakhir}
            </p>
          </div>
        </div>
        <div className="mt-5">
          <div className="float-left">
            <i className="fas fa-tags"></i> {props.harga}
          </div>
          <Link
            to={`/event/${props.nama}`}
            className="btn btn-primary float-right"
          >
            Lihat Event
          </Link>
        </div>
      </div>
    </div>
  );
};

export default CardEvent;
