import React from "react";
import "./CardAnggota.css";

const CardAnggota = (props) => {
  return (
    <div className="card text-center mt-5">
      <img className="card-img-top" src={props.avatar} alt="Avatar" />
      <div className="card-body">
        <h5 className="card-title">
          {props.nama_depan} {props.nama_belakang}
        </h5>
        <h5 className="card-text">{props.angkatan}</h5>
        <h5 className="card-text">{props.jabatan}</h5>
      </div>
    </div>
  );
};

export default CardAnggota;
