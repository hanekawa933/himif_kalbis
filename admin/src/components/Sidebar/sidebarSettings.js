import React from "react";
import { Link } from "react-router-dom";

export const adminSidebar = (
  <>
    <li className="sidebar__nav-item">
      <a
        data-toggle="collapse"
        href="#Anggota"
        role="button"
        aria-expanded="false"
        aria-controls="collapseExample"
        className="sidebar__nav-link"
      >
        <i className="fas fa-users sidebar__icon"></i>
        <span className="sidebar__textLink">Anggota</span>
      </a>
      <span className="sidebar__iconLeft">
        <i className="fas fa-chevron-left"></i>
      </span>
    </li>
    <div className="collapse" id="Anggota">
      <li className="sidebar__nav-item">
        <Link to="/dashboard/view/anggota" className="sidebar__nav-link">
          <i className="fas fa-eye sidebar__icon"></i>
          <span className="sidebar__textLink">Lihat Anggota</span>
        </Link>
      </li>
      <li className="sidebar__nav-item">
        <Link to="/dashboard/create/anggota" className="sidebar__nav-link">
          <i className="fas fa-plus sidebar__icon"></i>
          <span className="sidebar__textLink">Buat Anggota</span>
        </Link>
      </li>
    </div>
    <li className="sidebar__nav-item">
      <a
        data-toggle="collapse"
        href="#Event"
        role="button"
        aria-expanded="false"
        aria-controls="collapseExample"
        className="sidebar__nav-link"
      >
        <i className="fas fa-calendar-alt sidebar__icon"></i>
        <span className="sidebar__textLink">Event</span>
      </a>
      <span className="sidebar__iconLeft">
        <i className="fas fa-chevron-left"></i>
      </span>
    </li>
    <div className="collapse" id="Event">
      <li className="sidebar__nav-item">
        <Link to="/dashboard/view/event" className="sidebar__nav-link">
          <i className="fas fa-eye sidebar__icon"></i>
          <span className="sidebar__textLink">Lihat Event</span>
        </Link>
      </li>
      <li className="sidebar__nav-item">
        <Link to="/dashboard/create/event" className="sidebar__nav-link">
          <i className="fas fa-plus sidebar__icon"></i>
          <span className="sidebar__textLink">Buat Event</span>
        </Link>
      </li>
    </div>
    <li className="sidebar__nav-item">
      <a
        data-toggle="collapse"
        href="#Dokumen"
        role="button"
        aria-expanded="false"
        aria-controls="collapseExample"
        className="sidebar__nav-link"
      >
        <i className="fas fa-file-alt sidebar__icon"></i>
        <span className="sidebar__textLink">Dokumen</span>
      </a>
      <span className="sidebar__iconLeft">
        <i className="fas fa-chevron-left"></i>
      </span>
    </li>
    <div className="collapse" id="Dokumen">
      <li className="sidebar__nav-item">
        <Link to="/dashboard/view/dokumen" className="sidebar__nav-link">
          <i className="fas fa-eye icon__sidebar"></i>
          <span className="sidebar__textLink">Lihat Dokumen</span>
        </Link>
      </li>
      <li className="sidebar__nav-item">
        <Link to="/dashboard/create/dokumen" className="sidebar__nav-link">
          <i className="fas fa-plus sidebar__icon"></i>
          <span className="sidebar__textLink">Buat Dokumen</span>
        </Link>
      </li>
    </div>
  </>
);

export const anggotaSidebar = (
  <>
    <li className="sidebar__nav-item">
      <a
        data-toggle="collapse"
        href="#Anggota"
        role="button"
        aria-expanded="false"
        aria-controls="collapseExample"
        className="sidebar__nav-link"
      >
        <i className="fas fa-users sidebar__icon"></i>
        <span className="sidebar__textLink">Anggota</span>
      </a>
      <span className="sidebar__iconLeft">
        <i className="fas fa-chevron-left"></i>
      </span>
    </li>
    <div className="collapse" id="Anggota">
      <li className="sidebar__nav-item">
        <Link to="/dashboard/view/anggota" className="sidebar__nav-link">
          <i className="fas fa-eye sidebar__icon"></i>
          <span className="sidebar__textLink">Lihat Anggota</span>
        </Link>
      </li>
      <li className="sidebar__nav-item">
        <Link to="/dashboard/create/anggota" className="sidebar__nav-link">
          <i className="fas fa-plus sidebar__icon"></i>
          <span className="sidebar__textLink">Buat Anggota</span>
        </Link>
      </li>
    </div>
  </>
);

export const dokumenSidebar = (
  <>
    <li className="sidebar__nav-item">
      <a
        data-toggle="collapse"
        href="#Dokumen"
        role="button"
        aria-expanded="false"
        aria-controls="collapseExample"
        className="sidebar__nav-link"
      >
        <i className="fas fa-file-alt sidebar__icon"></i>
        <span className="sidebar__textLink">Dokumen</span>
      </a>
      <span className="sidebar__iconLeft">
        <i className="fas fa-chevron-left"></i>
      </span>
    </li>
    <div className="collapse" id="Dokumen">
      <li className="sidebar__nav-item">
        <Link to="/dashboard/view/dokumen" className="sidebar__nav-link">
          <i className="fas fa-eye icon__sidebar"></i>
          <span className="sidebar__textLink">Lihat Dokumen</span>
        </Link>
      </li>
      <li className="sidebar__nav-item">
        <Link to="/dashboard/create/dokumen" className="sidebar__nav-link">
          <i className="fas fa-plus sidebar__icon"></i>
          <span className="sidebar__textLink">Buat Dokumen</span>
        </Link>
      </li>
    </div>
  </>
);

export const eventSidebar = (
  <>
    <li className="sidebar__nav-item">
      <a
        data-toggle="collapse"
        href="#Event"
        role="button"
        aria-expanded="false"
        aria-controls="collapseExample"
        className="sidebar__nav-link"
      >
        <i className="fas fa-calendar-alt sidebar__icon"></i>
        <span className="sidebar__textLink">Event</span>
      </a>
      <span className="sidebar__iconLeft">
        <i className="fas fa-chevron-left"></i>
      </span>
    </li>
    <div className="collapse" id="Event">
      <li className="sidebar__nav-item">
        <Link to="/dashboard/view/event" className="sidebar__nav-link">
          <i className="fas fa-eye sidebar__icon"></i>
          <span className="sidebar__textLink">Lihat Event</span>
        </Link>
      </li>
      <li className="sidebar__nav-item">
        <Link to="/dashboard/create/event" className="sidebar__nav-link">
          <i className="fas fa-plus sidebar__icon"></i>
          <span className="sidebar__textLink">Buat Event</span>
        </Link>
      </li>
    </div>
  </>
);
