import React from "react";
import { Link, Redirect } from "react-router-dom";
import { logout } from "../../actions/auth";
import { connect } from "react-redux";
import Logo from "../../assets/images/logo.png";
import Avatar from "../../assets/images/img_avatar.png";
import "./Sidebar.css";
import {
  adminSidebar,
  eventSidebar,
  anggotaSidebar,
  dokumenSidebar,
} from "./sidebarSettings";

const Sidebar = ({ logout, isAuthenticated, user }) => {
  if (!isAuthenticated) {
    return <Redirect to="/admin" />;
  }

  const { anggotum } = user;

  const userDashboard = anggotum
    ? `${anggotum && anggotum.nama_depan} ${anggotum && anggotum.nama_belakang}`
    : "Administrators";

  return (
    <div className="sidebar">
      <div className="sidebar__logo">
        <Link to="/dashboard" className="sidebar__logoLink">
          <img src={Logo} alt="Logo" className="sidebar__image" />
          <span className="sidebar__logo__text">INFINITY KALBIS</span>
        </Link>
      </div>
      <div className="sidebar__user">
        <img src={Avatar} alt="Avatar" className="sidebar__avatar" />
        <span className="sidebar__avatar__text">{userDashboard}</span>
      </div>
      <div className="sidebar__list">
        <ul className="sidebar__nav-items">
          <li className="sidebar__nav-item">
            <Link to="/dashboard" className="sidebar__nav-link">
              <i className="fas fa-home sidebar__icon"></i>
              <span className="sidebar__textLink">Beranda</span>
            </Link>
          </li>
          {anggotum && anggotum.divisi.divisi === "Anggota"
            ? anggotaSidebar
            : anggotum && anggotum.divisi.divisi === "Dokumen"
            ? dokumenSidebar
            : anggotum && anggotum.divisi.divisi === "Event"
            ? eventSidebar
            : adminSidebar}
          <li className="sidebar__nav-item">
            <a
              data-toggle="collapse"
              href="#Profile"
              role="button"
              aria-expanded="false"
              aria-controls="collapseExample"
              className="sidebar__nav-link"
            >
              <i className="fas fa-user sidebar__icon"></i>
              <span className="sidebar__textLink">Profile</span>
            </a>
            <span className="sidebar__iconLeft">
              <i className="fas fa-chevron-left"></i>
            </span>
          </li>
          <div className="collapse" id="Profile">
            <li className="sidebar__nav-item">
              <Link
                to={`/dashboard/change/username/${user.username}`}
                className="sidebar__nav-link"
              >
                <i className="fas fa-id-card sidebar__icon"></i>
                <span className="sidebar__textLink">Ganti Username</span>
              </Link>
            </li>
            <li className="sidebar__nav-item">
              <Link
                to={`/dashboard/change/password/${user.username}`}
                className="sidebar__nav-link"
              >
                <i className="fas fa-lock sidebar__icon"></i>
                <span className="sidebar__textLink">Ganti Password</span>
              </Link>
            </li>
          </div>
          <li className="sidebar__nav-item">
            <Link to="/" onClick={logout} className="sidebar__nav-link">
              <i className="fas fa-sign-out-alt sidebar__icon"></i>
              <span className="sidebar__textLink">Keluar</span>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
  user: state.auth.user,
});

export default connect(mapStateToProps, { logout })(Sidebar);
