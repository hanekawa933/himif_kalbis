import React from "react";
import { Link } from "react-router-dom";
import Logo from "../../assets/images/logo.png";
import "./Navbar.css";

const Navbar = (props) => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark d-flex justify-content-evenly align-items-center w-100 navbar__background">
      <Link
        className="navbar-brand w-25 d-flex justify-content-center align-items-center"
        to="/"
      >
        <img src={Logo} alt="Infinity Logo" className="navbar__logo" />
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto w-100 navbar__custom d-flex justify-content-around">
          <li className={`nav-item nav-item-custom`}>
            <Link className="navbar__link" to="/">
              Home
            </Link>
          </li>
          <li className="nav-item nav-item-custom">
            <Link className="navbar__link" to="/anggota">
              Anggota
            </Link>
          </li>
          <li className="nav-item nav-item-custom">
            <Link className="navbar__link" to="/event">
              Event
            </Link>
          </li>
          <li className="nav-item nav-item-custom">
            <Link className="navbar__link" to="/dokumen">
              Dokumen
            </Link>
          </li>
          <li className="nav-item nav-item-custom">{props.link}</li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
