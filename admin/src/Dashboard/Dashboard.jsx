import React, { useEffect } from "react";
import Sidebar from "../components/Sidebar";
import "./Dashboard.css";
import { Link } from "react-router-dom";
import { getEvent, getAnggota, getDokumen } from "../actions/view";
import { connect } from "react-redux";
import Alert from "../components/Alert";
import DataTable from "react-data-table-component";

import Navbar from "../components/Navbar";

const Dashboard = ({
  anggota,
  event,
  dokumen,
  getAnggota,
  getEvent,
  getDokumen,
}) => {
  useEffect(() => {
    getEvent();
    getAnggota();
    getDokumen();
  }, [getEvent, getAnggota, getDokumen]);

  const events = event
    .map((eventProp, index) => {
      return {
        nama: eventProp.nama,
        crBy: eventProp.created_by,
        crAt: eventProp.createdAt,
        jenis: "Event",
      };
    })
    .slice(event.length - 3, event.length);

  const members = anggota
    .map((anggotaProp, index) => {
      return {
        nama: anggotaProp.nama_depan + " " + anggotaProp.nama_belakang,
        crBy: anggotaProp.created_by,
        crAt: anggotaProp.createdAt,
        jenis: "Anggota",
      };
    })
    .slice(anggota.length - 3, anggota.length);

  const documents = dokumen
    .map((dokumenProp, index) => {
      return {
        nama: dokumenProp.nama,
        crBy: dokumenProp.created_by,
        crAt: dokumenProp.createdAt,
        jenis: "Dokumen",
      };
    })
    .slice(dokumen.length - 3, dokumen.length);

  const combineData = [...members, ...events, ...documents];

  let id = 1;
  const data = combineData.map((newData, index) => {
    return {
      key: index,
      index: id++,
      nama: newData.nama,
      crBy: newData.crBy,
      crAt: newData.crAt,
      jenis: newData.jenis,
    };
  });

  const columns = [
    {
      name: "No",
      selector: "index",
      sortable: true,
    },
    {
      name: "Nama",
      selector: "nama",
      sortable: true,
    },
    {
      name: "Dibuat oleh",
      selector: "crBy",
      sortable: true,
      width: "200px",
      wrap: true,
    },
    {
      name: "Tanggal buat",
      selector: "crAt",
      sortable: true,
    },
    {
      name: "Jenis data",
      selector: "jenis",
      sortable: true,
      wrap: true,
    },
  ];

  const customStyles = {
    headCells: {
      style: {
        background: "#87B38D",
        color: "white",
      },
    },
  };

  const sumAnggota = anggota.length;
  const sumEvent = event.length;
  const sumDokumen = dokumen.length;

  return (
    <div className="dashboard">
      <div className="dashboard__sidebar">
        <Sidebar />
      </div>
      <div className="dashboard__content">
        <Navbar />
        <div className="dashboard__heading">Dashboard</div>
        <Link
          to="/dashboard/view/pesan"
          className="btn btn-primary dashboard__button"
        >
          Pesan
        </Link>
        <Alert />
        <div className="row m-0 p-0">
          <div className="col-lg-4 col-md-6 col-sm-12 dashboard__card__content">
            <div className="dashboard__card dashboard__card__bg1">
              <div className="dashboard__card__item">
                <div className="dashboard__card__text">
                  <p className="dashboard__card__itemName">Anggota</p>
                  <p className="dashboard__card__itemNumber">{sumAnggota}</p>
                </div>
                <i className="fas fa-users dashboard__card__icon"></i>
              </div>
              <Link to="/dashboard/view/anggota" className="dashboard__link">
                Lihat Data Anggota <i className="fas fa-arrow-right"></i>
              </Link>
            </div>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-12 dashboard__card__content">
            <div className="dashboard__card dashboard__card__bg2">
              <div className="dashboard__card__item">
                <div className="dashboard__card__text">
                  <p className="dashboard__card__itemName">Event</p>
                  <p className="dashboard__card__itemNumber">{sumEvent}</p>
                </div>
                <i className="fas fa-calendar-alt dashboard__card__icon"></i>
              </div>
              <Link to="/dashboard/view/event" className="dashboard__link">
                Lihat Data Event <i className="fas fa-arrow-right"></i>
              </Link>
            </div>
          </div>
          <div className="col-lg-4 col-md-12 col-sm-12 dashboard__card__content">
            <div className="dashboard__card dashboard__card__bg3">
              <div className="dashboard__card__item">
                <div className="dashboard__card__text">
                  <p className="dashboard__card__itemName">Dokumen</p>
                  <p className="dashboard__card__itemNumber">{sumDokumen}</p>
                </div>
                <i className="fas fa-file-alt dashboard__card__icon"></i>
              </div>
              <Link to="/dashboard/view/dokumen" className="dashboard__link">
                Lihat Data Dokumen <i className="fas fa-arrow-right"></i>
              </Link>
            </div>
          </div>
        </div>
        <div className="dashboard__latest">
          <DataTable
            pagination={true}
            columns={columns}
            data={data}
            customStyles={customStyles}
            title="Latest Entry"
          />
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  success: (state.create.success = false),
  successProfile: (state.profile.success = false),
  anggota: state.view.anggota,
  event: state.view.event,
  dokumen: state.view.dokumen,
});

export default connect(mapStateToProps, { getAnggota, getEvent, getDokumen })(
  Dashboard
);
