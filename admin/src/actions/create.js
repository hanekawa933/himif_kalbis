import {
  ANGGOTA_CREATED,
  EVENT_CREATED,
  DOKUMEN_CREATED,
  BELI_EVENT,
  SEND_MESSAGE,
} from "../types/create";

import { CREATE_ERROR } from "../types/fail";

import { setAlert } from "./alert";

import axios from "axios";

export const createAnggota = ({ formData }) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  try {
    const res = await axios.post("/api/anggota", formData, config);
    dispatch({
      type: ANGGOTA_CREATED,
      payload: res.data,
    });

    dispatch(setAlert("Anggota berhasil dibuat", "success"));
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, "danger")));
    }
    dispatch({
      type: CREATE_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });
  }
};

export const createEvent = ({ formData }) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  try {
    const res = await axios.post("/api/event", formData, config);
    dispatch({
      type: EVENT_CREATED,
      payload: res.data,
    });

    dispatch(setAlert("Event berhasil dibuat", "success"));
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, "danger")));
    }

    dispatch({
      type: CREATE_ERROR,
    });
  }
};

export const createDokumen = ({ formData }) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  };

  try {
    const res = await axios.post("/api/dokumen", formData, config);
    dispatch({
      type: DOKUMEN_CREATED,
      payload: res.data,
    });

    dispatch(setAlert("Dokumen berhasil dibuat", "success"));
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, "danger")));
    }
    dispatch({
      type: CREATE_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });
  }
};

export const beliEvent = (
  { nama_depan, nama_belakang, email, no_telp },
  event_name
) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const body = JSON.stringify({
    nama_depan,
    nama_belakang,
    email,
    no_telp,
  });

  try {
    const res = await axios.post(`/api/pengunjung/${event_name}`, body, config);
    dispatch({
      type: BELI_EVENT,
      payload: res.data,
    });

    dispatch(
      setAlert(
        "Event berhasil dipesan, silahkan cek email anda untuk validasi...",
        "success"
      )
    );
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, "danger")));
    }

    dispatch({
      type: CREATE_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });
  }
};

export const sendMessage = ({ nama, email, pesan }) => async (dispatch) => {
  const config = {
    headers: { "Content-Type": "application/json" },
  };

  const body = JSON.stringify({ nama, email, pesan });

  try {
    const res = await axios.post(`/api/pesan`, body, config);
    dispatch({
      type: SEND_MESSAGE,
      payload: res.data,
    });

    dispatch(
      setAlert("Pesan terkirim, terima kasih atas feedbacknya", "success")
    );
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, "danger")));
    }
    dispatch({
      type: CREATE_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });
  }
};
