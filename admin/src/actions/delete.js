import {
  EVENT_DELETED,
  DOKUMEN_DELETED,
  ANGGOTA_DELETED,
} from "../types/delete";
import { DELETE_ERROR } from "../types/fail";
import { getAnggota, getEvent, getDokumen } from "./view";
import axios from "axios";

export const deleteAnggota = (nim) => async (dispatch) => {
  try {
    const res = await axios.delete(`/api/anggota/${nim}`);
    dispatch({
      type: ANGGOTA_DELETED,
      payload: res.data,
    });
    dispatch(getAnggota());
  } catch (err) {
    dispatch({
      type: DELETE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};

export const deleteDokumen = (nama_dokumen) => async (dispatch) => {
  try {
    const res = await axios.delete(`/api/dokumen/${nama_dokumen}`);
    dispatch({
      type: DOKUMEN_DELETED,
      payload: res.data,
    });
    dispatch(getDokumen());
  } catch (err) {
    dispatch({
      type: DELETE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};

export const deleteEvent = (event_name) => async (dispatch) => {
  try {
    const res = await axios.delete(`/api/event/${event_name}`);
    dispatch({
      type: EVENT_DELETED,
      payload: res.data,
    });
    dispatch(getEvent());
  } catch (err) {
    dispatch({
      type: DELETE_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status },
    });
  }
};
