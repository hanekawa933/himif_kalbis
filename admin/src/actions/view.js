import {
  GET_ANGGOTA,
  GET_ALL_ANGGOTA,
  GET_EVENT,
  GET_ALL_EVENT,
  GET_DOKUMEN,
  GET_ALL_DOKUMEN,
  GET_PENGUNJUNG,
  GET_MESSAGE,
} from "../types/view";

import {
  GET_ANGGOTA_ERROR,
  GET_EVENT_ERROR,
  GET_DOKUMEN_ERROR,
  GET_ERROR,
} from "../types/fail";

import axios from "axios";

export const getAnggota = () => async (dispatch) => {
  try {
    const res = await axios.get("/api/anggota");
    dispatch({
      type: GET_ALL_ANGGOTA,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ANGGOTA_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });
  }
};

export const getSingleAnggota = (nim) => async (dispatch) => {
  try {
    const res = await axios.get(`/api/anggota/${nim}`);
    dispatch({
      type: GET_ANGGOTA,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ANGGOTA_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });
  }
};

export const getDokumen = () => async (dispatch) => {
  try {
    const res = await axios.get("/api/dokumen");
    dispatch({
      type: GET_ALL_DOKUMEN,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_DOKUMEN_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });
  }
};

export const getSingleDokumen = (nama_dokumen) => async (dispatch) => {
  try {
    const res = await axios.get(`/api/dokumen/${nama_dokumen}`);
    dispatch({
      type: GET_DOKUMEN,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_DOKUMEN_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });
  }
};

export const getEvent = () => async (dispatch) => {
  try {
    const res = await axios.get("/api/event");
    dispatch({
      type: GET_ALL_EVENT,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_EVENT_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });
  }
};

export const getPengunjung = (event_name) => async (dispatch) => {
  try {
    const res = await axios.get(`/api/pengunjung/${event_name}`);
    dispatch({
      type: GET_PENGUNJUNG,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });
  }
};

export const getSingleEvent = (nama_event) => async (dispatch) => {
  try {
    const res = await axios.get(`/api/event/${nama_event}`);
    dispatch({
      type: GET_EVENT,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_EVENT_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });
  }
};

export const getMessage = () => async (dispatch) => {
  try {
    const res = await axios.get("/api/pesan");

    dispatch({
      type: GET_MESSAGE,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });
  }
};
