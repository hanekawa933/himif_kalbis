import {
  ANGGOTA_UPDATED,
  EVENT_UPDATED,
  DOKUMEN_UPDATED,
  VALIDASI_EVENT,
} from "../types/update";

import { UPDATE_ERROR } from "../types/fail";

import axios from "axios";

import { setAlert } from "./alert";

export const updateAnggota = (
  { nama_depan, nama_belakang, nim, jabatan, angkatan, divisi_id },
  paramNims
) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const body = JSON.stringify({
    nama_depan,
    nama_belakang,
    nim,
    jabatan,
    divisi_id,
    angkatan,
  });

  try {
    const res = await axios.put(`/api/anggota/${paramNims}`, body, config);
    dispatch({
      type: ANGGOTA_UPDATED,
      payload: res.data,
    });
    dispatch(setAlert("Data berhasil diupdate", "success"));
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, "danger")));
    }

    dispatch({
      type: UPDATE_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });
  }
};

export const updateDokumen = (
  { nama, description, jenis_file, tipe_file },
  nama_dokumen
) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const body = JSON.stringify({
    nama,
    description,
    jenis_file,
    tipe_file,
  });

  try {
    const res = await axios.put(`/api/dokumen/${nama_dokumen}`, body, config);
    dispatch({
      type: DOKUMEN_UPDATED,
      payload: res.data,
    });
    dispatch(setAlert("Data berhasil diupdate", "success"));
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, "danger")));
    }

    dispatch({
      type: UPDATE_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });
  }
};

export const updateEvent = (
  {
    nama,
    description,
    harga,
    rules,
    lokasi,
    tanggal,
    jam_mulai,
    jam_berakhir,
    bangku_tersedia,
    nama_tamu,
  },
  nama_event
) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const body = JSON.stringify({
    nama,
    description,
    harga,
    rules,
    lokasi,
    tanggal,
    jam_mulai,
    jam_berakhir,
    bangku_tersedia,
    nama_tamu,
  });
  try {
    const res = await axios.put(`/api/event/${nama_event}`, body, config);
    dispatch({
      type: EVENT_UPDATED,
      payload: res.data,
    });
    dispatch(setAlert("Data berhasil diupdate", "success"));
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, "danger")));
    }

    dispatch({
      type: UPDATE_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });
  }
};

export const validasiPengunjung = (email, nama_event) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  try {
    const res = await axios.put(
      `/api/pengunjung/verify/${email}&event${nama_event}`,
      config
    );
    dispatch({
      type: VALIDASI_EVENT,
      payload: res.data,
    });

    dispatch(
      setAlert(
        "Validasi berhasil. Tiket sudah bisa digunakan. Redirect ke halaman event dalam 5 detik",
        "success"
      )
    );
  } catch (error) {
    dispatch({
      type: UPDATE_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });

    dispatch(
      setAlert(
        "Validasi gagal. email mungkin sudah digunakan atau belum terdaftar.",
        "danger"
      )
    );
  }
};
