import { CHANGE_USERNAME, CHANGE_PASSWORD } from "../types/profile";
import { CHANGE_ERROR } from "../types/fail";

import { setAlert } from "./alert";

import axios from "axios";

// Change Password
export const changePassword = (
  username,
  password,
  newPassword,
  retypePassword
) => async (dispatch) => {
  const config = {
    headers: { "Content-Type": "application/json" },
  };

  const body = JSON.stringify({ password, newPassword, retypePassword });

  try {
    const res = await axios.put(`/api/auth/password/${username}`, body, config);
    dispatch({
      type: CHANGE_PASSWORD,
      payload: res.data,
    });
    dispatch(setAlert("Password berhasil dirubah", "success"));
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, "danger")));
    }

    dispatch({
      type: CHANGE_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });
  }
};

// Change Password
export const changeUsername = (usernameParams, username) => async (
  dispatch
) => {
  const config = {
    headers: { "Content-Type": "application/json" },
  };

  const body = JSON.stringify({ username });

  try {
    const res = await axios.put(`/api/auth/${usernameParams}`, body, config);
    dispatch({
      type: CHANGE_USERNAME,
      payload: res.data,
    });
    dispatch(setAlert("Username berhasil dirubah", "success"));
  } catch (error) {
    dispatch({
      type: CHANGE_ERROR,
      payload: {
        msg: error.response.statusText,
        status: error.response.status,
      },
    });
  }
};
