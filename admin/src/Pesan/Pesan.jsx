import React, { useEffect } from "react";
import Sidebar from "../components/Sidebar";
import { getMessage } from "../actions/view";
import { connect } from "react-redux";
import DataTable from "react-data-table-component";

import Navbar from "../components/Navbar";

const Pesan = ({ getMessage, message }) => {
  useEffect(() => {
    getMessage();
  }, [getMessage]);

  let id = 1;

  const messages = message.map((msg, index) => {
    return {
      key: index,
      index: id++,
      nama: msg.nama,
      email: msg.email,
      pesan: msg.pesan,
    };
  });

  const columns = [
    {
      name: "No",
      selector: "index",
      sortable: true,
    },
    {
      name: "Nama Pengirim",
      selector: "nama",
      sortable: true,
    },
    {
      name: "Email",
      selector: "email",
      sortable: true,
    },
    {
      name: "Pesan",
      selector: "pesan",
      sortable: true,
    },
  ];

  const customStyles = {
    headCells: {
      style: {
        background: "#87B38D",
        color: "white",
      },
    },
  };

  return (
    <div className="view">
      <div className="view__sidebar">
        <Sidebar />
      </div>
      <div className="view__content">
        <Navbar />
        <div className="view__heading">Pesan Pengunjung</div>
        <div className="view__table">
          <DataTable
            pagination={true}
            columns={columns}
            data={messages}
            customStyles={customStyles}
          />
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  message: state.view.message,
});

export default connect(mapStateToProps, { getMessage })(Pesan);
