import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { changePassword } from "../../actions/profile";
import Sidebar from "../../components/Sidebar";
import "./ChangePassword.css";

import Navbar from "../../components/Navbar";
import Alert from "../../components/Alert";

const ChangePassword = ({ changePassword, success, match }) => {
  const [formData, setFormData] = useState({
    password: "",
    newPassword: "",
    retypePassword: "",
  });

  const { password, newPassword, retypePassword } = formData;

  const onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    changePassword(match.params.nim, password, newPassword, retypePassword);
  };

  if (success) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <div className="changePassword">
      <div className="changePassword__sidebar">
        <Sidebar />
      </div>
      <div className="changePassword__content">
        <Navbar />
        <div className="changePassword__heading">Ganti Password</div>
        <form className="changePassword__form" onSubmit={(e) => onSubmit(e)}>
          <Alert />
          <div className="form-group">
            <label htmlFor="Password Lama">Password Lama</label>
            <input
              type="password"
              name="password"
              className="form-control"
              onChange={(e) => onChange(e)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Password Password Baru">
              Password Password Baru
            </label>
            <input
              type="password"
              name="newPassword"
              className="form-control"
              onChange={(e) => onChange(e)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Konfirmasi Password Baru">
              Konfirmasi Password Baru
            </label>
            <input
              type="password"
              name="retypePassword"
              className="form-control"
              onChange={(e) => onChange(e)}
            />
          </div>
          <div className="form-group">
            <button className="btn btn-primary btn-block">
              Change Password
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  data: state.profile.data,
  success: state.profile.success,
});

export default connect(mapStateToProps, { changePassword })(ChangePassword);
