import React, { useState } from "react";
import Sidebar from "../../components/Sidebar";
import { Redirect } from "react-router-dom";
import { changeUsername } from "../../actions/profile";
import { connect } from "react-redux";
import "./ChangeUsername.css";
import Navbar from "../../components/Navbar";

const ChangeUsername = ({ changeUsername, success, match }) => {
  const [formData, setFormData] = useState({
    username: "",
    password: "",
  });

  const { username } = formData;

  const onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    changeUsername(match.params.nim, username);
  };

  if (success) {
    return <Redirect to="/dashboard" />;
  }
  return (
    <div className="changeUsername">
      <div className="changeUsername__sidebar">
        <Sidebar />
      </div>
      <div className="changeUsername__content">
        <Navbar />
        <div className="changeUsername__heading">Ganti Username</div>
        <form className="changeUsername__form" onSubmit={(e) => onSubmit(e)}>
          <div className="form-group">
            <label htmlFor="Username Baru Lama">Username Baru</label>
            <input
              type="text"
              name="username"
              className="form-control"
              onChange={(e) => onChange(e)}
            />
          </div>
          <div className="form-group">
            <button className="btn btn-primary btn-block">
              Change Username
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  data: state.profile.data,
  success: state.profile.success,
});

export default connect(mapStateToProps, { changeUsername })(ChangeUsername);
