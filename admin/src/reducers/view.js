import {
  GET_ANGGOTA,
  GET_ALL_ANGGOTA,
  GET_EVENT,
  GET_ALL_EVENT,
  GET_DOKUMEN,
  GET_ALL_DOKUMEN,
  GET_PENGUNJUNG,
  GET_MESSAGE,
} from "../types/view";
import {
  GET_ANGGOTA_ERROR,
  GET_EVENT_ERROR,
  GET_DOKUMEN_ERROR,
  GET_ERROR,
} from "../types/fail";

const initialState = {
  event: [],
  anggota: [],
  dokumen: [],
  message: [],
  pengunjung: [],
  data: [],
  success: null,
  errors: [],
};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_ANGGOTA:
    case GET_EVENT:
    case GET_DOKUMEN:
      return {
        ...state,
        data: payload,
        success: true,
      };

    case GET_MESSAGE:
      return {
        ...state,
        message: payload,
        success: true,
      };
    case GET_ALL_ANGGOTA:
      return {
        ...state,
        success: true,
        anggota: payload,
      };
    case GET_ALL_EVENT:
      return {
        ...state,
        success: true,
        event: payload,
      };
    case GET_ALL_DOKUMEN:
      return {
        ...state,
        success: true,
        dokumen: payload,
      };
    case GET_PENGUNJUNG:
      return {
        ...state,
        success: true,
        pengunjung: payload,
      };
    case GET_ANGGOTA_ERROR:
      return {
        ...state,
        success: false,
        anggota: [],
        errors: payload,
      };
    case GET_EVENT_ERROR:
      return {
        ...state,
        success: false,
        event: [],
        errors: payload,
      };
    case GET_DOKUMEN_ERROR:
      return {
        ...state,
        success: false,
        dokumen: [],
        errors: payload,
      };
    case GET_ERROR:
      return {
        ...state,
        success: false,
        pengunjung: [],
        errors: payload,
      };
    default:
      return state;
  }
}
