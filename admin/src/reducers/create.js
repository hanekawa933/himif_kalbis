import {
  ANGGOTA_CREATED,
  EVENT_CREATED,
  DOKUMEN_CREATED,
  BELI_EVENT,
  SEND_MESSAGE,
} from "../types/create";

import {
  ANGGOTA_UPDATED,
  EVENT_UPDATED,
  DOKUMEN_UPDATED,
  VALIDASI_EVENT,
} from "../types/update";

import { CREATE_ERROR, UPDATE_ERROR } from "../types/fail";

const initialState = {
  data: [],
  success: null,
  errors: [],
};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case SEND_MESSAGE:
    case DOKUMEN_UPDATED:
    case EVENT_UPDATED:
    case ANGGOTA_UPDATED:
    case DOKUMEN_CREATED:
    case EVENT_CREATED:
    case ANGGOTA_CREATED:
    case BELI_EVENT:
    case VALIDASI_EVENT:
      return {
        ...state,
        success: true,
        data: payload,
      };
    case UPDATE_ERROR:
    case CREATE_ERROR:
      return {
        ...state,
        success: false,
        data: [],
        errors: payload,
      };
    default:
      return state;
  }
}
