import { combineReducers } from "redux";

import auth from "./auth";
import alert from "./alert";
import create from "./create";
import profile from "./profile";
import view from "./view";

export default combineReducers({
  auth,
  alert,
  create,
  profile,
  view,
});
