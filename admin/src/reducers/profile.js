import { CHANGE_USERNAME, CHANGE_PASSWORD } from "../types/profile";
import { CHANGE_ERROR } from "../types/fail";

const initialState = {
  token: localStorage.getItem("token"),
  data: [],
  success: null,
  errors: [],
};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case CHANGE_USERNAME:
    case CHANGE_PASSWORD:
      localStorage.setItem("token", payload.token);
      return {
        ...state,
        success: true,
        data: payload,
      };
    case CHANGE_ERROR:
      return {
        ...state,
        success: false,
        data: [],
        errors: payload,
      };
    default:
      return state;
  }
}
