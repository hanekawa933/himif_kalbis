// ADMIN

import Login from "../Login";
import Dashboard from "../Dashboard";
import CreateAnggota from "../Anggota/CreateAnggota";
import CreateEvent from "../Event/CreateEvent";
import CreateDokumen from "../Dokumen/CreateDokumen";

import ChangeUsername from "../Profile/ChangeUsername";
import ChangePassword from "../Profile/ChangePassword";

import ViewAnggota from "../Anggota/ViewAnggota";
import ViewEvent from "../Event/ViewEvent";
import ViewDokumen from "../Dokumen/ViewDokumen";
import ViewPengunjung from "../Event/EventPengunjung";

import UpdateAnggota from "../Anggota/UpdateAnggota";
import UpdateDokumen from "../Dokumen/UpdateDokumen";
import UpdateEvent from "../Event/UpdateEvent";

// CLIENT
import Landing from "../Landing";
import Event from "../Event";
import Anggota from "../Anggota";
import Dokumen from "../Dokumen";
import EventDetail from "../Event/EventDetail";
import BeliEvent from "../Event/BeliEvent";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import ValidasiEvent from "../Event/ValidasiEvent";
import Kontak from "../Kontak";

import Pesan from "../Pesan";

export {
  // ADMIN

  Login,
  Dashboard,
  CreateAnggota,
  CreateEvent,
  CreateDokumen,
  ChangeUsername,
  ChangePassword,
  ViewAnggota,
  ViewEvent,
  ViewDokumen,
  ViewPengunjung,
  UpdateAnggota,
  UpdateEvent,
  UpdateDokumen,
  Pesan,
  // CLIENT
  Landing,
  Event,
  EventDetail,
  BeliEvent,
  Anggota,
  Dokumen,
  Navbar,
  Footer,
  ValidasiEvent,
  Kontak,
};
