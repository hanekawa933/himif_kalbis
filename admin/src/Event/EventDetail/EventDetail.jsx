import React, { useEffect } from "react";
import { connect } from "react-redux";
import { getSingleEvent } from "../../actions/view";
import "./EventDetail.css";
import NavbarClient from "../../components/NavbarClient";
import { Link } from "react-router-dom";
import Alert from "../../components/Alert";

const EventDetail = ({ getSingleEvent, match, dataEvent }) => {
  useEffect(() => {
    getSingleEvent(match.params.nama_event);
  }, [getSingleEvent, match.params.nama_event]);

  const poster = dataEvent.poster;
  const namaEvent = dataEvent.nama;
  const lokasi = dataEvent.lokasi;
  const harga = dataEvent.harga === 0 ? "Gratis" : dataEvent.harga;
  const mulai = dataEvent.jam_mulai;
  const berakhir = dataEvent.jam_berakhir;
  const tanggal = dataEvent.tanggal;
  const deskripsi = dataEvent.description;
  const rules = dataEvent.rules;
  const tersedia = dataEvent.bangku_tersedia;

  const setDate = new Date(tanggal);
  const month = setDate.toLocaleString("default", { month: "long" });
  const year = setDate.getFullYear();
  const date = setDate.getDay();
  const formatDate = `${(`0` + date).slice(-2)}/${month}/${year}`;

  const soldOut = (
    <Link to={`/event/${namaEvent}`} className="btn btn-primary">
      Sold Out
    </Link>
  );

  const available = (
    <Link to={`/event/beli/${namaEvent}`} className="btn btn-primary">
      Beli Tiket
    </Link>
  );

  const link = (
    <Link to="/contact" className="navbar__link">
      Contact
    </Link>
  );

  return (
    <>
      <NavbarClient link={link} />
      <div className="eventDetail">
        <img src={poster} alt="Event" className="eventDetail__image" />
        <div className="eventDetail__content">
          <Alert />
          <p className="eventDetail__title">{namaEvent}</p>
          <div className="row">
            <div className="col-lg-6 col-md-6 col-sm-12">
              <div className="eventDetail__detail">
                <i className="fas fa-map-marker-alt eventDetail__icon"></i>
                {lokasi}
              </div>
              <div className="eventDetail__detail">
                <i className="fas fa-tags eventDetail__icon"></i>
                {harga}
              </div>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12">
              <div className="eventDetail__detail">
                <i className="fas fa-clock eventDetail__icon"></i> {mulai} -{" "}
                {berakhir}
              </div>
              <div className="eventDetail__detail">
                <i className="fas fa-calendar-week eventDetail__icon"></i>{" "}
                {formatDate}
              </div>
            </div>
          </div>
          <div className="eventDetail__deskripsi">
            <p className="eventDetail__heading">Deskripsi Event</p>
            <p>{deskripsi}</p>
          </div>
          <div className="eventDetail__terms">
            <p className="eventDetail__heading">Syarat dan Ketentuan</p>
            <p>{rules}</p>
          </div>
          <div className="eventDetail__beli">
            <div className="eventDetail__detail">
              <p>
                <i className="fas fa-tags"></i> Tiket Tersedia: {tersedia}
              </p>
            </div>
            {tersedia > 0 ? available : soldOut}
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  dataEvent: state.view.data,
  success: (state.create.success = false),
});

export default connect(mapStateToProps, { getSingleEvent })(EventDetail);
