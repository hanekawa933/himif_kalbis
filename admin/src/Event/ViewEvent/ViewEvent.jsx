import React, { useEffect } from "react";
import Sidebar from "../../components/Sidebar";
import { getEvent } from "../../actions/view";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import DataTable from "react-data-table-component";
import { deleteEvent } from "../../actions/delete";

import Navbar from "../../components/Navbar";

const ViewEvent = ({ deleteEvent, getEvent, event, user }) => {
  useEffect(() => {
    getEvent();
  }, [getEvent]);

  let id = 1;

  const events = event.map((event, index) => {
    return {
      key: index,
      index: id++,
      nama: event.nama,
      desc: event.description,
      harga: event.harga,
      rules: event.rules,
      poster: (
        <img
          src={event.poster}
          className="my-3"
          alt="Poster"
          style={{
            width: "100px",
            height: "100px",
          }}
        />
      ),
      lokasi: event.lokasi,
      tanggal: `${event.tanggal}, ${event.jam_mulai} - ${event.jam_berakhir}`,
      delete: (
        <>
          <button
            type="button"
            class="btn btn-outline-danger"
            data-toggle="modal"
            data-target={`#index${index}`}
          >
            Delete
          </button>

          <div
            class="modal fade"
            id={`index${index}`}
            tabindex="-1"
            role="dialog"
            aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true"
          >
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  Apakah anda yakin ingin menghapus data ini?
                </div>
                <div class="modal-footer">
                  <button
                    type="button"
                    class="btn btn-secondary"
                    data-dismiss="modal"
                  >
                    Batal
                  </button>
                  <button
                    type="button"
                    class="btn btn-danger"
                    data-dismiss="modal"
                    onClick={(e) => deleteEvent(event.nama)}
                  >
                    Hapus
                  </button>
                </div>
              </div>
            </div>
          </div>
        </>
      ),
      update: (
        <Link
          to={`/dashboard/update/event/${event.nama}`}
          className="btn btn-outline-success"
        >
          Update
        </Link>
      ),
      dataPengunjung: (
        <Link
          to={`/dashboard/view/event/${event.nama}`}
          className="btn btn-outline-success"
        >
          Lihat Pengunjung
        </Link>
      ),
    };
  });

  const columns = [
    {
      name: "No",
      selector: "index",
      sortable: true,
    },
    {
      name: "Nama Event",
      selector: "nama",
      sortable: true,
    },
    {
      name: "Deskripsi Event",
      selector: "desc",
      sortable: true,
      width: "200px",
      wrap: true,
    },
    {
      name: "Harga",
      selector: "harga",
      sortable: true,
    },
    {
      name: "Rules",
      selector: "rules",
      sortable: true,
      wrap: true,
    },
    {
      name: "Poster",
      selector: "poster",
      sortable: true,
    },
    {
      name: "Lokasi",
      selector: "lokasi",
      sortable: true,
    },
    {
      name: "Tanggal",
      selector: "tanggal",
      sortable: true,
      wrap: true,
    },
    {
      name: "Delete",
      selector: "delete",
      sortable: true,
      width: "110px",
    },
    {
      name: "Update",
      selector: "update",
      sortable: true,
      width: "110px",
    },
    {
      name: "Data Pengunjung",
      selector: "dataPengunjung",
      sortable: true,
      width: "200px",
    },
  ];

  const customStyles = {
    headCells: {
      style: {
        background: "#87B38D",
        color: "white",
      },
    },
  };

  const { anggotum } = user;

  if (
    anggotum &&
    anggotum.divisi.divisi !== "Event" &&
    anggotum &&
    anggotum.divisi.divisi !== "Administrator"
  ) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <div className="view">
      <div className="view__sidebar">
        <Sidebar />
      </div>
      <div className="view__content">
        <Navbar />
        <div className="view__heading">Data Event</div>
        <div className="view__table">
          <DataTable
            pagination={true}
            columns={columns}
            data={events}
            customStyles={customStyles}
          />
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  event: state.view.event,
  user: state.auth.user,
});

export default connect(mapStateToProps, { getEvent, deleteEvent })(ViewEvent);
