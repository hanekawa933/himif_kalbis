import React, { useEffect } from "react";
import Sidebar from "../../components/Sidebar";
import { getPengunjung } from "../../actions/view";
import { connect } from "react-redux";
import DataTable from "react-data-table-component";

import Navbar from "../../components/Navbar";

const ViewEvent = ({ match, getPengunjung, pengunjung }) => {
  useEffect(() => {
    getPengunjung(match.params.event_name);
  }, [getPengunjung, match.params.event_name]);

  let id = 1;
  const visitors = pengunjung.map((visitor, index) => {
    return {
      key: index,
      index: id++,
      nama: `${visitor.nama_depan} ${visitor.nama_belakang}`,
      email: visitor.email,
      noTelp: visitor.no_telp,
    };
  });

  const namaEvent = match.params.event_name;

  const columns = [
    {
      name: "No",
      selector: "index",
      sortable: true,
    },
    {
      name: "Nama",
      selector: "nama",
      sortable: true,
    },
    {
      name: "Email",
      selector: "email",
      sortable: true,
    },
    {
      name: "No Telp",
      selector: "noTelp",
      sortable: true,
    },
  ];

  const customStyles = {
    headCells: {
      style: {
        background: "#272343",
        color: "white",
      },
    },
  };

  return (
    <div className="view">
      <div className="view__sidebar">
        <Sidebar />
      </div>
      <div className="view__content">
        <Navbar />
        <div className="view__heading">Data Pengunjung {namaEvent}</div>
        <div className="view__table">
          <DataTable
            pagination={true}
            columns={columns}
            data={visitors}
            customStyles={customStyles}
          />
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  pengunjung: state.view.pengunjung,
});

export default connect(mapStateToProps, { getPengunjung })(ViewEvent);
