import React, { useEffect } from "react";
import NavbarClient from "../../components/NavbarClient";
import { connect } from "react-redux";
import { validasiPengunjung } from "../../actions/update";

import Alert from "../../components/Alert";

const ValidasiEvent = ({ validasiPengunjung, success, match, ...props }) => {
  useEffect(() => {
    validasiPengunjung(match.params.email, match.params.nama_event);
  }, [validasiPengunjung, match.params.email, match.params.nama_event]);

  setTimeout(() => {
    props.history.push("/event");
  }, 5000);
  return (
    <div>
      <NavbarClient />
      <div className="row">
        <div className="col-6 w-100 mx-auto mt-5">
          <Alert />
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  success: state.create.success,
  data: state.create.data,
});

export default connect(mapStateToProps, { validasiPengunjung })(ValidasiEvent);
