import React, { useState, useEffect } from "react";
import Sidebar from "../../components/Sidebar";
import { Redirect } from "react-router-dom";
import { updateEvent } from "../../actions/update";
import { getSingleEvent } from "../../actions/view";
import { connect } from "react-redux";

import Navbar from "../../components/Navbar";

import Alert from "../../components/Alert";

const UpdateEvent = ({
  updateEvent,
  getSingleEvent,
  match,
  dataEvent,
  success,
  loading,
}) => {
  const [formData, setFormData] = useState({
    nama: "",
    description: "",
    harga: "",
    rules: "",
    lokasi: "",
    tanggal: "",
    jam_mulai: "",
    jam_berakhir: "",
    bangku_tersedia: "",
    nama_tamu: "",
  });

  const {
    nama,
    description,
    harga,
    rules,
    lokasi,
    tanggal,
    jam_mulai,
    jam_berakhir,
    bangku_tersedia,
    nama_tamu,
  } = formData;

  useEffect(() => {
    getSingleEvent(match.params.nama_event);

    setFormData({
      nama: dataEvent.nama,
      description: dataEvent.description,
      harga: dataEvent.harga,
      rules: dataEvent.rules,
      lokasi: dataEvent.lokasi,
      tanggal: dataEvent.tanggal,
      jam_mulai: dataEvent.jam_mulai,
      jam_berakhir: dataEvent.jam_berakhir,
      bangku_tersedia: dataEvent.bangku_tersedia,
      nama_tamu: dataEvent.tamu && dataEvent.tamu.nama_tamu,
    });
  }, [
    dataEvent.nama,
    dataEvent.description,
    dataEvent.harga,
    dataEvent.rules,
    dataEvent.lokasi,
    dataEvent.tanggal,
    dataEvent.jam_mulai,
    dataEvent.jam_berakhir,
    dataEvent.bangku_tersedia,
    dataEvent.tamu && dataEvent.tamu.nama_tamu,
  ]);

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async (e) => {
    e.preventDefault();
    updateEvent(
      {
        nama,
        description,
        harga,
        rules,
        lokasi,
        tanggal,
        jam_mulai,
        jam_berakhir,
        bangku_tersedia,
        nama_tamu,
      },
      match.params.nama_event
    );
  };

  if (success) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <div className="event">
      <div className="event__sidebar">
        <Sidebar />
      </div>
      <div className="event__content">
        <Navbar />
        <div className="event__heading">Update Event</div>
        <form className="event__form" onSubmit={(e) => onSubmit(e)}>
          <Alert />
          <div className="form-group">
            <label htmlFor="Nama Event">Nama Event</label>
            <input
              type="text"
              name="nama"
              className="form-control"
              onChange={(e) => onChange(e)}
              value={nama}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Deskripsi Event">Deskripsi</label>
            <input
              type="text"
              name="description"
              className="form-control"
              onChange={(e) => onChange(e)}
              value={description}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Rules">Rules</label>
            <input
              type="text"
              name="rules"
              className="form-control"
              onChange={(e) => onChange(e)}
              value={rules}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Tamu">Tamu</label>
            <input
              type="text"
              name="nama_tamu"
              className="form-control"
              onChange={(e) => onChange(e)}
              value={nama_tamu}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Lokasi">Lokasi</label>
            <input
              type="text"
              name="lokasi"
              className="form-control"
              onChange={(e) => onChange(e)}
              value={lokasi}
            />
          </div>
          <div className="row">
            <div className="col-lg-6 col-12">
              <div className="form-group">
                <label htmlFor="Tanggal">Tanggal</label>
                <input
                  type="date"
                  name="tanggal"
                  className="form-control"
                  onChange={(e) => onChange(e)}
                  value={tanggal}
                />
              </div>
            </div>
            <div className="col-lg-3 col-6">
              <div className="form-group">
                <label htmlFor="Jam Mulai">Jam Mulai</label>
                <input
                  type="time"
                  name="jam_mulai"
                  className="form-control"
                  onChange={(e) => onChange(e)}
                  value={jam_mulai}
                />
              </div>
            </div>
            <div className="col-lg-3 col-6">
              <div className="form-group">
                <label htmlFor="Jam Berakhir">Jam Berakhir</label>
                <input
                  type="time"
                  name="jam_berakhir"
                  className="form-control"
                  onChange={(e) => onChange(e)}
                  value={jam_berakhir}
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-6 col-12">
              <div className="form-group">
                <label htmlFor="Harga">Harga</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Rp.</div>
                  </div>
                  <input
                    type="text"
                    name="harga"
                    className="form-control"
                    onChange={(e) => onChange(e)}
                    value={harga}
                  />
                </div>
              </div>
            </div>
            <div className="col-lg-6 col-12">
              <div className="form-group">
                <label htmlFor="Bangku Tersedia">Bangku Tersedia</label>
                <input
                  type="number"
                  name="bangku_tersedia"
                  className="form-control"
                  onChange={(e) => onChange(e)}
                  value={bangku_tersedia}
                />
              </div>
            </div>
          </div>
          <div className="form-group">
            <button className="btn btn-primary btn-block">Buat Event</button>
          </div>
        </form>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  dataEvent: state.view.data,
  data: state.create.data,
  success: state.create.success,
});

export default connect(mapStateToProps, { updateEvent, getSingleEvent })(
  UpdateEvent
);
