import React, { useEffect } from "react";
import { connect } from "react-redux";
import { getEvent } from "../actions/view";
import NavbarClient from "../components/NavbarClient";
import CardEvent from "../components/Card/CardEvent";
import "./Event.css";
import { Link } from "react-router-dom";

const Event = ({ getEvent, event }) => {
  useEffect(() => {
    getEvent();
  }, [getEvent]);

  let id = 1;

  const events = event.map((eventProp, index) => {
    if (new Date(eventProp.tanggal) > new Date().getTime()) {
      return (
        <div className="col-lg-6 col-12">
          <CardEvent
            key={id++}
            nama={eventProp.nama}
            lokasi={eventProp.lokasi}
            tanggal={eventProp.tanggal}
            mulai={eventProp.jam_mulai}
            berakhir={eventProp.jam_berakhir}
            harga={eventProp.harga === 0 ? "Gratis" : eventProp.harga}
            poster={eventProp.poster}
          />
        </div>
      );
    }
    return null;
  });

  const link = (
    <Link to="/contact" className="navbar__link">
      Contact
    </Link>
  );

  return (
    <>
      <NavbarClient link={link} />
      <div className="event__heading">Event berlangsung</div>
      <div className="event__client">
        <div className="row">{events}</div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  event: state.view.event,
});

export default connect(mapStateToProps, { getEvent })(Event);
