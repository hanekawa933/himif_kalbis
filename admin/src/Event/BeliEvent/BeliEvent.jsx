import React, { useState } from "react";
import NavbarClient from "../../components/NavbarClient";
import "./BeliEvent.css";
import { beliEvent } from "../../actions/create";
import { connect } from "react-redux";
import { Redirect, Link } from "react-router-dom";
import Alert from "../../components/Alert";

const BeliEvent = ({ match, beliEvent, success }) => {
  const [formData, setFormData] = useState({
    nama_depan: "",
    nama_belakang: "",
    email: "",
    no_telp: "",
  });
  const { nama_depan, nama_belakang, email, no_telp } = formData;

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async (e) => {
    e.preventDefault();
    beliEvent(
      {
        nama_depan,
        nama_belakang,
        email,
        no_telp,
      },
      match.params.nama_event
    );
  };

  if (success) {
    return <Redirect to={`/event/${match.params.nama_event}`} />;
  }

  const link = (
    <Link to="/contact" className="navbar__link">
      Contact
    </Link>
  );

  return (
    <div>
      <NavbarClient link={link} />
      <div className="booking">
        <div className="booking__heading">Daftar Event</div>
        <form className="form" onSubmit={(e) => onSubmit(e)}>
          <Alert />
          <div className="row">
            <div className="col-lg-6 col-md-6 col-sm-12">
              <div className="form-group">
                <label htmlFor="Nama Depan">Nama Depan </label>
                <input
                  type="text"
                  className="form-control"
                  name="nama_depan"
                  onChange={(e) => onChange(e)}
                />
              </div>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12">
              <div className="form-group">
                <label htmlFor="Nama Belakang">Nama Belakang </label>
                <input
                  type="text"
                  className="form-control"
                  name="nama_belakang"
                  onChange={(e) => onChange(e)}
                />
              </div>
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="Email">Email</label>
            <input
              type="text"
              className="form-control"
              name="email"
              onChange={(e) => onChange(e)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Nomor Telepon">Nomor Telepon</label>
            <input
              type="text"
              className="form-control"
              name="no_telp"
              onChange={(e) => onChange(e)}
            />
          </div>
          <div className="form-group">
            <button className="btn btn-primary form-control">
              Daftar Event
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  data: state.create.data,
  success: state.create.success,
});

export default connect(mapStateToProps, { beliEvent })(BeliEvent);
