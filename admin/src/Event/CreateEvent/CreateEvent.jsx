import React, { useState } from "react";
import "./CreateEvent.css";
import Sidebar from "../../components/Sidebar";
import { Redirect } from "react-router-dom";
import { createEvent } from "../../actions/create";
import { connect } from "react-redux";

import Alert from "../../components/Alert";
import Navbar from "../../components/Navbar";

const CreateEvent = ({ createEvent, success }) => {
  const [formData, setFormData] = useState({
    nama: "",
    description: "",
    harga: "0",
    rules: "",
    poster: "",
    lokasi: "",
    tanggal: "",
    jam_mulai: "",
    jam_berakhir: "",
    bangku_tersedia: "",
    nama_tamu: "",
  });

  const {
    nama,
    description,
    harga,
    rules,
    poster,
    lokasi,
    tanggal,
    jam_mulai,
    jam_berakhir,
    bangku_tersedia,
    nama_tamu,
  } = formData;

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onFile = (e) => setFormData({ ...formData, poster: e.target.files[0] });

  const onSubmit = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("poster", poster);
    formData.append("nama", nama);
    formData.append("description", description);
    formData.append("harga", harga);
    formData.append("rules", rules);
    formData.append("lokasi", lokasi);
    formData.append("tanggal", tanggal);
    formData.append("jam_mulai", jam_mulai);
    formData.append("jam_berakhir", jam_berakhir);
    formData.append("bangku_tersedia", bangku_tersedia);
    formData.append("nama_tamu", nama_tamu);
    createEvent({
      formData,
    });
  };

  if (success) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <div className="event">
      <div className="event__sidebar">
        <Sidebar />
      </div>
      <div className="event__content">
        <Navbar />
        <div className="event__heading">Buat Event</div>
        <form className="event__form" onSubmit={(e) => onSubmit(e)}>
          <Alert />
          <div className="form-group">
            <label htmlFor="Nama Event">Nama Event</label>
            <input
              type="text"
              name="nama"
              className="form-control"
              onChange={(e) => onChange(e)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Deskripsi Event">Deskripsi</label>
            <input
              type="text"
              name="description"
              className="form-control"
              onChange={(e) => onChange(e)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Rules">Rules</label>
            <input
              type="text"
              name="rules"
              className="form-control"
              onChange={(e) => onChange(e)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Tamu">Tamu</label>
            <input
              type="text"
              name="nama_tamu"
              className="form-control"
              onChange={(e) => onChange(e)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Lokasi">Lokasi</label>
            <input
              type="text"
              name="lokasi"
              className="form-control"
              onChange={(e) => onChange(e)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Poster">Poster</label>
            <input
              type="file"
              name="poster"
              className="form-control-file"
              onChange={(e) => onFile(e)}
            />
          </div>
          <div className="row">
            <div className="col-lg-6 col-12">
              <div className="form-group">
                <label htmlFor="Tanggal">Tanggal</label>
                <input
                  type="date"
                  name="tanggal"
                  className="form-control"
                  onChange={(e) => onChange(e)}
                />
              </div>
            </div>
            <div className="col-lg-3 col-6">
              <div className="form-group">
                <label htmlFor="Jam Mulai">Jam Mulai</label>
                <input
                  type="time"
                  name="jam_mulai"
                  className="form-control"
                  onChange={(e) => onChange(e)}
                />
              </div>
            </div>
            <div className="col-lg-3 col-6">
              <div className="form-group">
                <label htmlFor="Jam Berakhir">Jam Berakhir</label>
                <input
                  type="time"
                  name="jam_berakhir"
                  className="form-control"
                  onChange={(e) => onChange(e)}
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-6 col-12">
              <div className="form-group">
                <label htmlFor="Harga">Harga</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Rp.</div>
                  </div>
                  <input
                    type="text"
                    name="harga"
                    className="form-control"
                    onChange={(e) => onChange(e)}
                    value={harga}
                  />
                </div>
              </div>
            </div>
            <div className="col-lg-6 col-12">
              <div className="form-group">
                <label htmlFor="Bangku Tersedia">Bangku Tersedia</label>
                <input
                  type="number"
                  name="bangku_tersedia"
                  className="form-control"
                  onChange={(e) => onChange(e)}
                />
              </div>
            </div>
          </div>
          <div className="form-group">
            <button className="btn btn-primary btn-block">Buat Event</button>
          </div>
        </form>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  data: state.create.data,
  success: state.create.success,
});

export default connect(mapStateToProps, { createEvent })(CreateEvent);
