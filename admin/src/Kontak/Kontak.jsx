import React, { useState } from "react";
import NavbarClient from "../components/NavbarClient";
import "../Event/BeliEvent/BeliEvent.css";
import { sendMessage } from "../actions/create";
import { connect } from "react-redux";
import { Redirect, Link } from "react-router-dom";
import Alert from "../components/Alert";

const Kontak = ({ sendMessage, success }) => {
  const [formData, setFormData] = useState({
    nama: "",
    email: "",
    pesan: "",
  });

  const { nama, email, pesan } = formData;

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async (e) => {
    e.preventDefault();
    sendMessage({
      nama,
      email,
      pesan,
    });
  };

  if (success) {
    return <Redirect to="/" />;
  }

  const link = (
    <Link to="/contact" className="navbar__link">
      Contact
    </Link>
  );

  return (
    <div>
      <NavbarClient link={link} />
      <div className="booking">
        <div className="booking__heading">Kirim Pesan</div>
        <form className="form" onSubmit={(e) => onSubmit(e)}>
          <Alert />
          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12">
              <div className="form-group">
                <label htmlFor="Nama Lengkap">Nama Lengkap </label>
                <input
                  type="text"
                  className="form-control"
                  name="nama"
                  onChange={(e) => onChange(e)}
                />
              </div>
            </div>
            <div className="col-lg-12 col-md-12 col-sm-12">
              <div className="form-group">
                <label htmlFor="Email">Email </label>
                <input
                  type="text"
                  className="form-control"
                  name="email"
                  onChange={(e) => onChange(e)}
                />
              </div>
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="Pesan">Pesan</label>
            <textarea
              type="text"
              className="form-control"
              name="pesan"
              onChange={(e) => onChange(e)}
              rows="5"
            />
          </div>
          <div className="form-group">
            <button className="btn btn-primary form-control">
              Kirim Pesan
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  data: state.create.data,
  success: state.create.success,
});

export default connect(mapStateToProps, { sendMessage })(Kontak);
