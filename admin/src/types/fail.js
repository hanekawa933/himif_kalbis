export const CREATE_ERROR = "CREATE_ERROR";
export const CHANGE_ERROR = "CHANGE_ERROR";
export const GET_ANGGOTA_ERROR = "GET_ANGGOTA_ERROR";
export const GET_EVENT_ERROR = "GET_EVENT_ERROR";
export const GET_DOKUMEN_ERROR = "GET_DOKUMEN_ERROR";
export const GET_ERROR = "GET_ERROR";
export const DELETE_ERROR = "DELETE_ERROR";
export const UPDATE_ERROR = "UPDATE_ERROR";
