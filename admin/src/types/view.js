export const GET_ANGGOTA = "GET_ANGGOTA";
export const GET_ALL_ANGGOTA = "GET_ALL_ANGGOTA";
export const GET_EVENT = "GET_EVENT";
export const GET_ALL_EVENT = "GET_ALL_EVENT";
export const GET_DOKUMEN = "GET_DOKUMEN";
export const GET_ALL_DOKUMEN = "GET_ALL_DOKUMEN";

export const GET_PENGUNJUNG = "GET_PENGUNJUNG";
export const GET_MESSAGE = "GET_MESSAGE";
