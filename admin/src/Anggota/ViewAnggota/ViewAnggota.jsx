import React, { useEffect } from "react";
import Sidebar from "../../components/Sidebar";
import { getAnggota } from "../../actions/view";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import DataTable from "react-data-table-component";
import "./View.css";
import { deleteAnggota } from "../../actions/delete";

import Navbar from "../../components/Navbar";

const ViewAnggota = ({ deleteAnggota, getAnggota, anggota, user }) => {
  useEffect(() => {
    getAnggota();
  }, [getAnggota]);

  let id = 1;

  const members = anggota.map((member, index) => {
    return {
      key: index,
      index: id++,
      namaLengkap: `${member.nama_depan} ${member.nama_belakang}`,
      nim: member.nim,
      jabatan: member.jabatan,
      angkatan: member.angkatan,
      divisi: member.divisi.divisi,
      foto: (
        <img
          className="my-3"
          src={member.foto}
          alt="Anggota"
          style={{ width: "100px", height: "100px" }}
        />
      ),
      delete: (
        <>
          <button
            type="button"
            className="btn btn-outline-danger"
            data-toggle="modal"
            data-target={`#index${index}`}
          >
            Delete
          </button>

          <div
            className="modal fade"
            id={`index${index}`}
            tabindex="-1"
            role="dialog"
            aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                  Apakah anda yakin ingin menghapus data ini?
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-dismiss="modal"
                  >
                    Batal
                  </button>
                  <button
                    type="button"
                    className="btn btn-danger"
                    data-dismiss="modal"
                    onClick={(e) => deleteAnggota(member.nim)}
                  >
                    Hapus
                  </button>
                </div>
              </div>
            </div>
          </div>
        </>
      ),
      update: (
        <Link
          to={`/dashboard/update/anggota/${member.nim}`}
          className="btn btn-outline-primary"
        >
          Update
        </Link>
      ),
    };
  });

  const columns = [
    {
      name: "No",
      selector: "index",
      sortable: true,
    },
    {
      name: "Nama Lengkap",
      selector: "namaLengkap",
      sortable: true,
      wrap: true,
      width: "150px",
    },
    {
      name: "NIM",
      selector: "nim",
      sortable: true,
    },
    {
      name: "Jabatan",
      selector: "jabatan",
      sortable: true,
    },
    {
      name: "Angkatan",
      selector: "angkatan",
      sortable: true,
    },
    {
      name: "Divisi",
      selector: "divisi",
      sortable: true,
    },
    {
      name: "Foto",
      selector: "foto",
      sortable: true,
    },
    {
      name: "Delete",
      selector: "delete",
      sortable: true,
    },
    {
      name: "Update",
      selector: "update",
      sortable: true,
    },
  ];

  const customStyles = {
    headCells: {
      style: {
        background: "#87B38D",
        color: "white",
      },
    },
  };

  const { anggotum } = user;

  if (
    anggotum &&
    anggotum.divisi.divisi !== "Anggota" &&
    anggotum &&
    anggotum.divisi.divisi !== "Administrator"
  ) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <div className="view">
      <div className="view__sidebar">
        <Sidebar />
      </div>
      <div className="view__content">
        <Navbar />
        <div className="view__heading">Data Anggota</div>
        <div className="view__table">
          <DataTable
            pagination={true}
            columns={columns}
            data={members}
            customStyles={customStyles}
          />
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  anggota: state.view.anggota,
  user: state.auth.user,
});

export default connect(mapStateToProps, { getAnggota, deleteAnggota })(
  ViewAnggota
);
