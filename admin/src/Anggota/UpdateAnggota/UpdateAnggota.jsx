import React, { useState, useEffect } from "react";
import Sidebar from "../../components/Sidebar";
import { connect } from "react-redux";
import { updateAnggota } from "../../actions/update";
import { getSingleAnggota } from "../../actions/view";
import { Redirect } from "react-router-dom";

import Navbar from "../../components/Navbar";

import Alert from "../../components/Alert";

const UpdateAnggota = ({
  getSingleAnggota,
  match,
  success,
  updateAnggota,
  dataAnggota,
}) => {
  const [formData, setFormData] = useState({
    nama_depan: "",
    nama_belakang: "",
    nim: "",
    jabatan: "",
    angkatan: "",
    divisi_id: "",
  });

  const {
    nama_depan,
    nama_belakang,
    nim,
    jabatan,
    angkatan,
    divisi_id,
  } = formData;

  useEffect(() => {
    getSingleAnggota(match.params.nim);

    setFormData({
      nama_depan: dataAnggota.nama_depan,
      nama_belakang: dataAnggota.nama_belakang,
      nim: dataAnggota.nim,
      jabatan: dataAnggota.jabatan,
      angkatan: dataAnggota.angkatan,
      divisi_id:
        dataAnggota.divisi && dataAnggota.divisi.divisi === "Administrator"
          ? 1
          : dataAnggota.divisi && dataAnggota.divisi.divisi === "Event"
          ? 2
          : dataAnggota.divisi && dataAnggota.divisi.divisi === "Anggota"
          ? 3
          : dataAnggota.divisi && dataAnggota.divisi.divisi === "Dokumen"
          ? 4
          : 5,
    });
  }, [
    dataAnggota.nama_depan,
    dataAnggota.nama_belakang,
    dataAnggota.nim,
    dataAnggota.jabatan,
    dataAnggota.angkatan,
    dataAnggota.divisi && dataAnggota.divisi.divisi,
  ]);

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const cekDivisi = dataAnggota.divisi && dataAnggota.divisi.divisi;

  const divisiAdmin = (
    <select
      name="divisi_id"
      className="form-control"
      onChange={(e) => onChange(e)}
      value={divisi_id}
    >
      <option value="1">Administrator</option>
      <option value="2">Event</option>
      <option value="3">Anggota</option>
      <option value="4">Dokumen</option>
      <option value="5">Tidak Berdivisi</option>
    </select>
  );

  const nonDivisi = (
    <select
      name="divisi_id"
      className="form-control"
      onChange={(e) => onChange(e)}
      value={divisi_id}
    >
      <option value="5">Tidak Berdivisi</option>
      <option value="1">Administrator</option>
      <option value="2">Event</option>
      <option value="3">Anggota</option>
      <option value="4">Dokumen</option>
    </select>
  );

  const divisiEvent = (
    <select
      name="divisi_id"
      className="form-control"
      onChange={(e) => onChange(e)}
      value={divisi_id}
    >
      <option value="2">Event</option>
      <option value="1">Administrator</option>
      <option value="3">Anggota</option>
      <option value="4">Dokumen</option>
      <option value="5">Tidak Berdivisi</option>
    </select>
  );

  const divisiAnggota = (
    <select
      name="divisi_id"
      className="form-control"
      onChange={(e) => onChange(e)}
      value={divisi_id}
    >
      <option value="3">Anggota</option>
      <option value="1">Administrator</option>
      <option value="2">Event</option>
      <option value="4">Dokumen</option>
      <option value="5">Tidak Berdivisi</option>
    </select>
  );

  const divisiDokumen = (
    <select
      name="divisi_id"
      className="form-control"
      onChange={(e) => onChange(e)}
      value={divisi_id}
    >
      <option value="4">Dokumen</option>
      <option value="1">Administrator</option>
      <option value="2">Event</option>
      <option value="3">Anggota</option>
      <option value="5">Tidak Berdivisi</option>
    </select>
  );

  const cekJabatan = dataAnggota.jabatan;

  const ketua = (
    <select
      name="jabatan"
      className="form-control"
      onChange={(e) => onChange(e)}
      value={jabatan}
    >
      <option value="Ketua">Ketua</option>
      <option value="Sekretaris">Sekretaris</option>
      <option value="Bendahara">Bendahara</option>
      <option value="Anggota">Anggota</option>
    </select>
  );

  const sekretaris = (
    <select
      name="jabatan"
      className="form-control"
      onChange={(e) => onChange(e)}
      value={jabatan}
    >
      <option value="Sekretaris">Sekretaris</option>
      <option value="Ketua">Ketua</option>
      <option value="Bendahara">Bendahara</option>
      <option value="Anggota">Anggota</option>
    </select>
  );

  const bendahara = (
    <select
      name="jabatan"
      className="form-control"
      onChange={(e) => onChange(e)}
      value={jabatan}
    >
      <option value="Bendahara">Bendahara</option>
      <option value="Ketua">Ketua</option>
      <option value="Sekretaris">Sekretaris</option>
      <option value="Anggota">Anggota</option>
    </select>
  );

  const anggota = (
    <select
      name="jabatan"
      className="form-control"
      onChange={(e) => onChange(e)}
      value={jabatan}
    >
      <option value="Anggota">Anggota</option>
      <option value="Ketua">Ketua</option>
      <option value="Sekretaris">Sekretaris</option>
      <option value="Bendahara">Bendahara</option>
    </select>
  );

  const onSubmit = async (e) => {
    e.preventDefault();
    updateAnggota(
      {
        nama_depan,
        nama_belakang,
        nim,
        jabatan,
        angkatan,
        divisi_id,
      },
      match.params.nim
    );
  };

  if (success) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <div className="anggota">
      <div className="anggota__sidebar">
        <Sidebar />
      </div>
      <div className="anggota__content">
        <Navbar />
        <div className="anggota__heading">Update Anggota</div>
        <form className="form" onSubmit={(e) => onSubmit(e)}>
          <Alert />
          <div className="form-group">
            <label htmlFor="NIM">NIM</label>
            <input
              type="text"
              name="nim"
              className="form-control"
              onChange={(e) => onChange(e)}
              value={nim}
            />
          </div>
          <div className="row">
            <div className="col-lg-6 col-12">
              <div className="form-group">
                <label htmlFor="Nama Depan">Nama Depan</label>
                <input
                  type="text"
                  name="nama_depan"
                  className="form-control"
                  onChange={(e) => onChange(e)}
                  value={nama_depan}
                />
              </div>
            </div>
            <div className="col-lg-6 col-12">
              <div className="form-group">
                <label htmlFor="Nama Belakang">Nama Belakang</label>
                <input
                  type="text"
                  name="nama_belakang"
                  className="form-control"
                  onChange={(e) => onChange(e)}
                  value={nama_belakang}
                />
              </div>
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="Jabatan">Jabatan</label>
            {cekJabatan === "Ketua"
              ? ketua
              : cekJabatan === "Bendahara"
              ? bendahara
              : cekJabatan === "Sekretaris"
              ? sekretaris
              : anggota}
          </div>
          <div className="form-group">
            <label htmlFor="Divisi">Divisi</label>
            {cekDivisi === "Administrator"
              ? divisiAdmin
              : cekDivisi === "Event"
              ? divisiEvent
              : cekDivisi === "Dokumen"
              ? divisiDokumen
              : cekDivisi === "Anggota"
              ? divisiAnggota
              : nonDivisi}
          </div>
          <div className="form-group">
            <label htmlFor="Angkatan">Angkatan</label>
            <input
              type="text"
              name="angkatan"
              className="form-control"
              onChange={(e) => onChange(e)}
              value={angkatan}
            />
          </div>
          <div className="form-group">
            <button className="btn btn-primary btn-block">Buat Anggota</button>
          </div>
        </form>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  dataAnggota: state.view.data,
  data: state.create.data,
  success: state.create.success,
});

export default connect(mapStateToProps, { updateAnggota, getSingleAnggota })(
  UpdateAnggota
);
