import React, { useState } from "react";
import Sidebar from "../../components/Sidebar";
import "./CreateAnggota.css";
import { connect } from "react-redux";
import { createAnggota } from "../../actions/create";
import { Redirect } from "react-router-dom";

import Alert from "../../components/Alert";

import Navbar from "../../components/Navbar";

const DateTodayYear = new Date(Date.now()).getFullYear();

const CreateAnggota = ({ success, createAnggota }) => {
  const [formData, setFormData] = useState({
    nama_depan: "",
    nama_belakang: "",
    nim: "",
    jabatan: "Ketua",
    foto: "",
    angkatan: DateTodayYear,
    divisi_id: 1,
  });

  const {
    nama_depan,
    nama_belakang,
    nim,
    jabatan,
    angkatan,
    divisi_id,
    foto,
  } = formData;

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onFile = (e) => setFormData({ ...formData, foto: e.target.files[0] });

  const onSubmit = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("nim", nim);
    formData.append("foto", foto);
    formData.append("nama_depan", nama_depan);
    formData.append("nama_belakang", nama_belakang);
    formData.append("jabatan", jabatan);
    formData.append("angkatan", angkatan);
    formData.append("divisi_id", divisi_id);
    createAnggota({
      formData,
    });
  };

  if (success) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <div className="anggota">
      <div className="anggota__sidebar">
        <Sidebar />
      </div>
      <div className="anggota__content">
        <Navbar />
        <div className="anggota__heading">Buat Anggota</div>
        <form className="form" onSubmit={(e) => onSubmit(e)}>
          <Alert />
          <div className="form-group">
            <label htmlFor="NIM">NIM</label>
            <input
              type="text"
              name="nim"
              className="form-control"
              onChange={(e) => onChange(e)}
              value={nim}
            />
          </div>
          <div className="row">
            <div className="col-lg-6 col-12">
              <div className="form-group">
                <label htmlFor="Nama Depan">Nama Depan</label>
                <input
                  type="text"
                  name="nama_depan"
                  className="form-control"
                  onChange={(e) => onChange(e)}
                  value={nama_depan}
                />
              </div>
            </div>
            <div className="col-lg-6 col-12">
              <div className="form-group">
                <label htmlFor="Nama Belakang">Nama Belakang</label>
                <input
                  type="text"
                  name="nama_belakang"
                  className="form-control"
                  onChange={(e) => onChange(e)}
                  value={nama_belakang}
                />
              </div>
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="Jabatan">Jabatan</label>
            <select
              name="jabatan"
              className="form-control"
              onChange={(e) => onChange(e)}
            >
              <option value="Ketua">Ketua</option>
              <option value="Sekretaris">Sekretaris</option>
              <option value="Bendahara">Bendahara</option>
              <option value="Anggota">Anggota</option>
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="Divisi">Divisi</label>
            <select
              name="divisi_id"
              className="form-control"
              onChange={(e) => onChange(e)}
            >
              <option value="1">Administrator</option>
              <option value="2">Anggota</option>
              <option value="3">Acara</option>
              <option value="4">Dokumen</option>
              <option value="5">Tidak Berdivisi</option>
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="Angkatan">Angkatan</label>
            <input
              type="text"
              name="angkatan"
              className="form-control"
              onChange={(e) => onChange(e)}
              value={angkatan}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Foto">Foto</label>
            <input
              type="file"
              name="foto"
              className="form-control-file"
              onChange={(e) => onFile(e)}
            />
          </div>
          <div className="form-group">
            <button className="btn btn-primary btn-block">Buat Anggota</button>
          </div>
        </form>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  data: state.create.data,
  success: state.create.success,
});

export default connect(mapStateToProps, { createAnggota })(CreateAnggota);
