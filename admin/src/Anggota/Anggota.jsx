import React, { useEffect } from "react";
import NavbarClient from "../components/NavbarClient";
import "./Anggota.css";
import { getAnggota } from "../actions/view";
import { connect } from "react-redux";
import CardAnggota from "../components/Card/CardAnggota";
import { Link } from "react-router-dom";

const Anggota = ({ getAnggota, anggota }) => {
  useEffect(() => {
    getAnggota();
  }, [getAnggota]);

  let id = 1;

  const members = anggota.map((anggotaProp, index) => {
    return (
      <div className="col-lg-4 col-md-6 col-sm-12">
        <CardAnggota
          key={id++}
          avatar={anggotaProp.foto}
          nama_depan={anggotaProp.nama_depan}
          nama_belakang={anggotaProp.nama_belakang}
          angkatan={anggotaProp.angkatan}
          jabatan={anggotaProp.jabatan}
        />
      </div>
    );
  });

  const link = (
    <Link to="/contact" className="navbar__link">
      Contact
    </Link>
  );

  return (
    <>
      <NavbarClient link={link} />
      <div className="anggota__heading">Anggota HMJ Informatika</div>
      <div className="anggota__client">
        <div className="row">{members}</div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  anggota: state.view.anggota,
});

export default connect(mapStateToProps, { getAnggota })(Anggota);
