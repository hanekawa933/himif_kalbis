import React, { useEffect } from "react";
import NavbarClient from "../components/NavbarClient";
import { connect } from "react-redux";
import { getDokumen } from "../actions/view";
import CardDokumen from "../components/Card/CardDokumen";
import "./Dokumen.css";
import { Link } from "react-router-dom";

const Dokumen = ({ getDokumen, dokumen }) => {
  useEffect(() => {
    getDokumen();
  }, [getDokumen]);

  let id = 1;

  const documents = dokumen.map((dokumenProp, index) => {
    return (
      <div className="col-lg-4 col-md-6 col-sm-12">
        <CardDokumen
          key={id++}
          icon={
            dokumenProp.tipe_file === "pdf"
              ? "fas fa-file-pdf cardDokumen__pdf"
              : "fas fa-file-word cardDokumen__word"
          }
          file={dokumenProp.file}
          nama={dokumenProp.nama}
        />
      </div>
    );
  });

  const link = (
    <Link to="/contact" className="navbar__link">
      Contact
    </Link>
  );

  return (
    <>
      <NavbarClient link={link} />
      <div className="dokumen__heading">Dokumen HMJ Informatika</div>
      <div className="dokumen__client">
        <div className="row">{documents}</div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  dokumen: state.view.dokumen,
});

export default connect(mapStateToProps, { getDokumen })(Dokumen);
