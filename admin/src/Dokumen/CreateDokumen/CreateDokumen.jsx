import React, { useState } from "react";
import Sidebar from "../../components/Sidebar";
import "./CreateDokumen.css";
import { Redirect } from "react-router-dom";
import { createDokumen } from "../../actions/create";
import { connect } from "react-redux";

import Alert from "../../components/Alert";

import Navbar from "../../components/Navbar";

const CreateDokumen = ({ createDokumen, success }) => {
  const [formData, setFormData] = useState({
    nama: "",
    description: "",
    jenis_file: "Rahasia",
    tipe_file: "pdf",
    file: "",
  });

  const { nama, description, jenis_file, tipe_file, file } = formData;

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onFile = (e) => setFormData({ ...formData, file: e.target.files[0] });

  const onSubmit = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("file", file);
    formData.append("nama", nama);
    formData.append("description", description);
    formData.append("jenis_file", jenis_file);
    formData.append("tipe_file", tipe_file);
    createDokumen({
      formData,
    });
  };

  if (success) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <div className="dokumen">
      <div className="dokumen__sidebar">
        <Sidebar />
      </div>
      <div className="dokumen__content">
        <Navbar />
        <div className="dokumen__heading">Buat Dokumen</div>
        <form className="dokumen__form" onSubmit={(e) => onSubmit(e)}>
          <Alert />
          <div className="form-group">
            <label htmlFor="Nama Dokumen">Nama Dokumen</label>
            <input
              type="text"
              name="nama"
              className="form-control"
              onChange={(e) => onChange(e)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Deskripsi Dokumen">Deskripsi</label>
            <input
              type="text"
              name="description"
              className="form-control"
              onChange={(e) => onChange(e)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Jenis File">Jenis File</label>
            <select
              name="jenis_file"
              className="form-control"
              onChange={(e) => onChange(e)}
            >
              <option value="Rahasia">Rahasia</option>
              <option value="Umum">Umum</option>
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="Tipe File">Tipe File</label>
            <select
              name="tipe_file"
              className="form-control"
              onChange={(e) => onChange(e)}
            >
              <option value="pdf">.PDF</option>
              <option value="docx">.DOCX</option>
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="File">File</label>
            <input
              type="file"
              name="file"
              className="form-control-file"
              onChange={(e) => onFile(e)}
            />
          </div>
          <div className="form-group">
            <button className="btn btn-primary btn-block">Buat Dokumen</button>
          </div>
        </form>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  data: state.create.data,
  success: state.create.success,
});

export default connect(mapStateToProps, { createDokumen })(CreateDokumen);
