import React, { useEffect } from "react";
import Sidebar from "../../components/Sidebar";
import { getDokumen } from "../../actions/view";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import DataTable from "react-data-table-component";
import { deleteDokumen } from "../../actions/delete";

import Navbar from "../../components/Navbar";

const ViewDokumen = ({ deleteDokumen, getDokumen, dokumen, user }) => {
  useEffect(() => {
    getDokumen();
  }, [getDokumen]);

  const { anggotum } = user;

  if (
    anggotum &&
    anggotum.divisi.divisi !== "Dokumen" &&
    anggotum &&
    anggotum.divisi.divisi !== "Administrator"
  ) {
    return <Redirect to="/dashboard" />;
  }

  let id = 1;

  const documents = dokumen.map((document, index) => {
    return {
      key: index,
      index: id++,
      nama: document.nama,
      desc: document.description,
      jenis_file: document.jenis_file,
      tipe_file: document.tipe_file,
      file: (
        <a href={document.file} className="btn btn-outline-primary" download>
          Download File
        </a>
      ),
      delete: (
        <>
          <button
            type="button"
            className="btn btn-outline-danger"
            data-toggle="modal"
            data-target={`#delete${index}`}
          >
            Delete
          </button>

          <div
            className="modal fade"
            id={`delete${index}`}
            tabindex="-1"
            role="dialog"
            aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                  Apakah anda yakin ingin menghapus data ini?
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-dismiss="modal"
                  >
                    Batal
                  </button>
                  <button
                    type="button"
                    className="btn btn-danger"
                    data-dismiss="modal"
                    onClick={(e) => deleteDokumen(document.nama)}
                  >
                    Hapus
                  </button>
                </div>
              </div>
            </div>
          </div>
        </>
      ),
      update: (
        <Link
          to={`/dashboard/update/dokumen/${document.nama}`}
          className="btn btn-outline-success my-3"
        >
          Update
        </Link>
      ),
    };
  });

  const columns = [
    {
      name: "No",
      selector: "index",
      sortable: true,
    },
    {
      name: "Nama Dokumen",
      selector: "nama",
      sortable: true,
      wrap: true,
      width: "150px",
    },
    {
      name: "Deskripsi Dokumen",
      selector: "desc",
      sortable: true,
      width: "200px",
    },
    {
      name: "Jenis File",
      selector: "jenis_file",
      sortable: true,
    },
    {
      name: "Tipe File",
      selector: "tipe_file",
      sortable: true,
    },
    {
      name: "File",
      selector: "file",
      sortable: true,
      width: "200px",
    },
    {
      name: "Delete",
      selector: "delete",
      sortable: true,
    },
    {
      name: "Update",
      selector: "update",
      sortable: true,
    },
  ];

  const customStyles = {
    headCells: {
      style: {
        background: "#87B38D",
        color: "white",
      },
    },
  };

  return (
    <div className="view">
      <div className="view__sidebar">
        <Sidebar />
      </div>
      <div className="view__content">
        <Navbar />
        <div className="view__heading">Data Dokumen</div>
        <div className="view__table">
          <DataTable
            pagination={true}
            columns={columns}
            data={documents}
            customStyles={customStyles}
          />
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  dokumen: state.view.dokumen,
  user: state.auth.user,
});

export default connect(mapStateToProps, { getDokumen, deleteDokumen })(
  ViewDokumen
);
