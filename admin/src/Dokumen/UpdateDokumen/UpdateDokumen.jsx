import React, { useState, useEffect } from "react";
import Sidebar from "../../components/Sidebar";
import { Redirect } from "react-router-dom";
import { updateDokumen } from "../../actions/update";
import { getSingleDokumen } from "../../actions/view";
import { connect } from "react-redux";

import Alert from "../../components/Alert";
import Navbar from "../../components/Navbar";

const CreateDokumen = ({
  updateDokumen,
  getSingleDokumen,
  match,
  dataDokumen,
  success,
}) => {
  const [formData, setFormData] = useState({
    nama: "",
    description: "",
    jenis_file: "",
    tipe_file: "",
  });

  const { nama, description, jenis_file, tipe_file } = formData;

  useEffect(() => {
    getSingleDokumen(match.params.nama_dokumen);

    setFormData({
      nama: dataDokumen.nama,
      description: dataDokumen.description,
      jenis_file: dataDokumen.jenis_file,
      tipe_file: dataDokumen.tipe_file,
    });
  }, [
    dataDokumen.nama,
    dataDokumen.description,
    dataDokumen.jenis_file,
    dataDokumen.tipe_file,
  ]);

  const cekJenis = dataDokumen.jenis_file;

  const rahasia = (
    <select
      name="jenis_file"
      className="form-control"
      onChange={(e) => onChange(e)}
    >
      <option value="Rahasia">Rahasia</option>
      <option value="Umum">Umum</option>
    </select>
  );

  const umum = (
    <select
      name="jenis_file"
      className="form-control"
      onChange={(e) => onChange(e)}
    >
      <option value="Umum">Umum</option>
      <option value="Rahasia">Rahasia</option>
    </select>
  );

  const cekTipe = dataDokumen.tipe_file;

  const pdf = (
    <select
      name="tipe_file"
      className="form-control"
      onChange={(e) => onChange(e)}
    >
      <option value="pdf">.PDF</option>
      <option value="docx">.DOCX</option>
    </select>
  );

  const docx = (
    <select
      name="tipe_file"
      className="form-control"
      onChange={(e) => onChange(e)}
    >
      <option value="docx">.DOCX</option>
      <option value="pdf">.PDF</option>
    </select>
  );

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async (e) => {
    e.preventDefault();
    updateDokumen(
      {
        nama,
        description,
        jenis_file,
        tipe_file,
      },
      match.params.nama_dokumen
    );
  };

  if (success) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <div className="dokumen">
      <div className="dokumen__sidebar">
        <Sidebar />
      </div>
      <div className="dokumen__content">
        <Navbar />
        <div className="dokumen__heading">Update Dokumen</div>
        <form className="dokumen__form" onSubmit={(e) => onSubmit(e)}>
          <Alert />
          <div className="form-group">
            <label htmlFor="Nama Dokumen">Nama Dokumen</label>
            <input
              type="text"
              name="nama"
              className="form-control"
              onChange={(e) => onChange(e)}
              value={nama}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Deskripsi Dokumen">Deskripsi</label>
            <input
              type="text"
              name="description"
              className="form-control"
              onChange={(e) => onChange(e)}
              value={description}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Jenis File">Jenis File</label>
            {cekJenis === "Rahasia" ? rahasia : umum}
          </div>
          <div className="form-group">
            <label htmlFor="Tipe File">Tipe File</label>
            {cekTipe === "pdf" ? pdf : docx}
          </div>
          <div className="form-group">
            <button className="btn btn-primary btn-block">Buat Dokumen</button>
          </div>
        </form>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  dataDokumen: state.view.data,
  data: state.create.data,
  success: state.create.success,
});

export default connect(mapStateToProps, { updateDokumen, getSingleDokumen })(
  CreateDokumen
);
