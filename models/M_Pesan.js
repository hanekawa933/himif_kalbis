const { DataTypes } = require("sequelize");
const db = require("../database/database");

const { notNullString, notNullText } = require("../utils/modelType");

const M_Pesan = db.define(
  "pesan",
  {
    nama: notNullString(),
    email: notNullString(),
    pesan: notNullText(),
  },
  {
    freezeTableName: true,
  }
);

module.exports = M_Pesan;
