const { DataTypes } = require("sequelize");
const db = require("../database/database");

const {
  notNullString,
  nullableString,
  notNullInteger,
} = require("../utils/modelType");

const M_Divisi = db.define(
  "divisi",
  {
    divisi: notNullString(100),
  },
  {
    freezeTableName: true,
  }
);

const M_Anggota = db.define(
  "anggota",
  {
    nama_depan: notNullString(35),
    nama_belakang: notNullString(35),
    nim: notNullString(15),
    jabatan: notNullString(50),
    angkatan: notNullString(4),
    divisi_id: notNullInteger(),
    foto: notNullString(255),
    created_by: notNullString(35),
    updated_by: nullableString(35),
  },
  {
    freezeTableName: true,
  }
);

M_Anggota.belongsTo(M_Divisi, {
  foreignKey: "divisi_id",
});

M_Divisi.hasMany(M_Anggota, {
  foreignKey: "divisi_id",
});

module.exports = {
  M_Anggota,
  M_Divisi,
};
