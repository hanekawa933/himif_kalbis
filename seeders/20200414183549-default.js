"use strict";

const bcrypt = require("bcryptjs");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return await Promise.all([
      queryInterface.bulkInsert("administrator", [
        {
          username: "admin",
          password: await bcrypt.hash("admin", await bcrypt.genSalt(12)),
          nama: "Administrator",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]),

      queryInterface.bulkInsert("divisi", [
        {
          divisi: "Administrator",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          divisi: "Event",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          divisi: "Anggota",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          divisi: "Dokumen",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          divisi: "None",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return await Promise.all([
      queryInterface.bulkDelete("administrator", null, {}),
      queryInterface.bulkDelete("divisi", null, {}),
    ]);
  },
};
