const express = require("express");
const router = express.Router();
const nodemailer = require("nodemailer");
const { check, validationResult } = require("express-validator");

const auth = require("../../middleware/auth");
const { M_Administrator, M_Admin_Divisi } = require("../../models/M_SuperUser");

const {
  M_Event,
  M_Pengunjung,
  M_Tiket,
  M_Tamu,
} = require("../../models/M_Event");

// Post Data Pengunjung
router.post(
  "/:event_name",
  [
    check("nama_depan", "Field nama depan dokumen wajib di isi")
      .not()
      .isEmpty(),
    check("nama_belakang", "Field nama belakang dokumen wajib di isi")
      .not()
      .isEmpty(),
    check("email", "Field email dokumen wajib di isi")
      .not()
      .isEmpty()
      .isEmail()
      .withMessage("Masukkan email yang valid"),
    check("no_telp", "Field nomor telepon dokumen wajib di isi")
      .not()
      .isEmpty()
      .isNumeric()
      .withMessage("Masukkan nomor telepon yang valid"),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { nama_depan, nama_belakang, email, no_telp } = req.body;
    try {
      const event_name = await M_Event.findOne({
        where: { nama: req.params.event_name },
      });

      const cekPengunjung = await M_Pengunjung.findOne({
        where: { email, event_id: event_name.id },
      });

      if (cekPengunjung) {
        res.status(400).json({
          errors: [
            {
              msg: "Alamat email sudah digunakan di event ini...",
            },
          ],
        });
      } else {
        const pengunjung = await M_Pengunjung.create({
          nama_depan,
          nama_belakang,
          email,
          no_telp,
          event_id: event_name.id,
        });

        const tiket = await M_Tiket.create({
          pengunjung_id: pengunjung.id,
        });

        delete pengunjung.dataValues[("id", "event_id")];
        delete tiket.dataValues["id"];

        const data = { ...pengunjung.dataValues, ...tiket.dataValues };
        res.status(200).send(data);
      }

      // create reusable transporter object using the default SMTP transport
      let transporter = nodemailer.createTransport({
        service: "gmail",
        secure: false, // true for 465, false for other ports
        auth: {
          user: "iqbal.ramadhan9933@gmail.com", // generated ethereal user
          pass: "hanekawa933", // generated ethereal password
        },
      });
      // send mail with defined transport object
      let info = await transporter.sendMail(
        {
          from: `"Iqbal Ramadhan" <iqbal.ramadhan9933@gmail.com>`, // sender address
          to: `${email}`, // list of receivers
          subject: "Menyelesaikan Pendaftaran Tiket", // Subject line
          text: "Hello world?", // plain text body
          html: `<h3>Halo, ${nama_depan} ${nama_belakang}</h3>
              <h3>Terima kasih atas pendaftaran tiket ${req.params.event_name} anda. Mohon untuk memastikan bahwa data dibawah ini benar.</h3>
              <h3>Nama Lengkap : ${nama_depan} ${nama_belakang}</h3>
              <h3>Email : ${email}</h3>
              <h3>Nomor Telepon : ${no_telp}</h3>
              <h3>Jika data tersebut benar, tolong klik link dibawah ini untuk menyelesaikan pendaftaran tiket anda.</h3> 
              <a href="http://localhost:3000/event/verifikasi/${email}&event${req.params.event_name}"><h3>Finish Purchasement</h3></a>.<br><h3>Salam hormat, <br>HMJ Informatika<br>Kalbis Institute</h3>`, // html body
        },
        (err, data) => {
          if (err) {
            console.log("Error", err);
          } else {
            console.log("Email has been sent", data);
          }
        }
      );
    } catch (error) {
      console.error(error.message);
      res.status(500).send("Server Error");
    }
  }
);

router.put("/verify/:email&event:nama", async (req, res) => {
  try {
    const event = await M_Event.findOne({
      where: { nama: req.params.nama },
    });

    const pengunjung = await M_Pengunjung.findOne({
      where: { email: req.params.email, event_id: event.id },
    });

    if (pengunjung && pengunjung.validasi !== "Aktif") {
      const verify = await M_Pengunjung.update(
        { validasi: "Aktif" },
        { where: { email: req.params.email } }
      );

      const events = await M_Event.decrement("bangku_tersedia", {
        by: 1,
        where: { id: pengunjung.event_id },
      });

      // create reusable transporter object using the default SMTP transport
      let transporter = nodemailer.createTransport({
        service: "gmail",
        secure: false, // true for 465, false for other ports
        auth: {
          user: "iqbal.ramadhan9933@gmail.com", // generated ethereal user
          pass: "hanekawa933", // generated ethereal password
        },
      });
      // send mail with defined transport object
      let info = await transporter.sendMail(
        {
          from: `"Iqbal Ramadhan" <iqbal.ramadhan9933@gmail.com>`, // sender address
          to: `${pengunjung.email}`, // list of receivers
          subject: "Registrasi Berhasil", // Subject line
          text: "Hello world?", // plain text body
          html: `<h3>Halo, ${pengunjung.nama_depan} ${pengunjung.nama_belakang}</h3>
                <h3>Terima kasih atas registrasi tiket ${event.nama} anda. Mohon untuk memastikan bahwa pesan email ini 
                tidak terhapus untuk dijadikan bukti pendaftaran dan registrasi anda untuk ditunjukan kepada panitia acara.</h3>
                <h3>Email : ${pengunjung.email}</h3>
                <h3>Nama Lengkap : ${pengunjung.nama_depan} ${pengunjung.nama_belakang}</h3>
                <h3>Email : ${pengunjung.no_telp}</h3>
                <h3>Telah terdaftar sebagai peserta acara ${event.nama}</h3>
                <br><h3>Salam hormat, <br>HMJ Informatika<br>Kalbis Institute</h3>`, // html body
        },
        (err, data) => {
          if (err) {
            console.log("Error", err);
          } else {
            console.log("Email has been sent", data);
          }
        }
      );
      res.status(200).send("Verifikasi Berhasil");
    } else {
      res
        .status(404)
        .send("Pengunjung sudah terverifikasi atau pengunjung tidak terdaftar");
    }
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

// Get Data Pengunjung
router.get("/", async (req, res) => {
  try {
    const pengunjung = await M_Pengunjung.findAll({
      include: [
        {
          model: M_Event,
          required: true,
          attributes: { exclude: ["id"] },
        },
      ],
      attributes: { exclude: ["id", "event_id"] },
    });
    res.json(pengunjung);
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

router.get("/:nama_event", async (req, res) => {
  try {
    const event = await M_Event.findOne({
      where: { nama: req.params.nama_event },
    });
    const pengunjung = await M_Pengunjung.findAll({
      where: { event_id: event.id, validasi: "Aktif" },
      attributes: { exclude: ["id"] },
    });

    res.status(200).send(pengunjung);
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

// Get Single Data Pengunjung
router.get("/:email", async (req, res) => {
  try {
    const check = await M_Pengunjung.findOne({
      where: { email: req.params.email },
    });
    if (!check) {
      res
        .status(400)
        .send({ errors: { msg: "There is no Pengunjung with that email" } });
    } else {
      const pengunjung = await M_Pengunjung.findOne({
        include: [
          {
            model: M_Event,
            required: true,
            attributes: { exclude: ["id"] },
          },
        ],
        attributes: { exclude: ["id"] },
      });
      res.json(pengunjung);
    }
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

// Update Data Pengunjung
router.put("/:email", async (req, res) => {
  const { nama_depan, nama_belakang, email, no_telp } = req.body;
  try {
    const check = await M_Pengunjung.findOne({
      where: { email: req.params.email },
    });
    if (!check) {
      res
        .status(400)
        .send({ errors: { msg: "There is no Pengunjung with that email" } });
    } else {
      const adminId = await M_Administrator.findByPk(req.user.id);
      const pengunjung = await M_Pengunjung.update(
        {
          nama_depan,
          nama_belakang,
          email,
          no_telp,
        },
        { where: { email: req.params.email } }
      );
      res.status(200).send("Pengunjung Successfully Updated");
    }
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

// Delete Data Pengunjung
router.delete("/:id", auth, async (req, res) => {
  try {
    const check = await M_Pengunjung.findOne({
      where: { id: req.params.id },
    });
    if (!check) {
      res
        .status(400)
        .send({ errors: { msg: "There is no Pengunjung with that ID" } });
    } else {
      const pengunjung = await M_Pengunjung.destroy({
        where: { id: req.params.id },
      });
      res.status(200).send("Pengunjung Successfully Deleted");
    }
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

module.exports = router;
