const express = require("express");
const router = express.Router();
const multer = require("multer");
const bcrypt = require("bcryptjs");
const { check, validationResult } = require("express-validator");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./public/images/");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "_" + file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb("Something Error", false);
  }
};

var upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 10,
  },
  fileFilter: fileFilter,
});

const auth = require("../../middleware/auth");
const { M_Administrator, M_Admin_Divisi } = require("../../models/M_SuperUser");

const { M_Anggota, M_Divisi } = require("../../models/M_Anggota");

const { Op } = require("sequelize");

//Create Data Anggota
router.post(
  "/",
  upload.single("foto"),
  [
    check("nama_depan", "Field nama depan wajib di isi").not().isEmpty(),
    check("nama_belakang", "Field nama belakang wajib di isi").not().isEmpty(),
    check("nim", "Field  NIM wajib di isi").not().isEmpty(),
    check("jabatan", "Field jabatan wajib di isi").not().isEmpty(),
    check("angkatan")
      .not()
      .isEmpty()
      .withMessage("Field angkatan wajib di isi")
      .isLength({ min: 4, max: 4 })
      .withMessage("Field angkatan harus 4 angka")
      .isNumeric()
      .withMessage("Field angkatan harus berupa angka"),
    check("divisi_id", "Field divisi wajib di isi").not().isEmpty(),
  ],
  auth,
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      nama_depan,
      nama_belakang,
      nim,
      jabatan,
      angkatan,
      divisi_id,
    } = req.body;

    try {
      const check = await M_Anggota.findOne({
        where: { nim },
      });

      if (check) {
        res.status(400).json({
          errors: [
            {
              msg: "NIM sudah terpakai",
            },
          ],
        });
      } else {
        const admin = await M_Administrator.findOne({
          where: { username: req.user.username },
          attributes: { exclude: ["id"] },
        });

        const divisi = await M_Admin_Divisi.findOne({
          where: { username: req.user.username },
          attributes: { exclude: ["id", "anggota_id"] },
          include: [
            {
              model: M_Anggota,
              required: true,
              attributes: { exclude: ["id", "divisi_id"] },
              include: [
                {
                  model: M_Divisi,
                  required: true,
                  attributes: { exclude: ["id"] },
                },
              ],
            },
          ],
        });

        if (admin) {
          const anggota = await M_Anggota.create({
            nama_depan,
            nama_belakang,
            nim,
            jabatan,
            angkatan,
            divisi_id,
            foto: "/images/" + req.file.filename,
            created_by: admin.nama,
          });

          if (anggota.divisi_id === 5) {
            delete anggota.dataValues[("id", "divisi_id")];
            res.status(200).send(anggota);
          } else {
            const createdBy = await M_Administrator.findOne({
              where: { username: req.user.username },
            });
            const anggotaId = await M_Anggota.findOne({ where: { nim } });
            const salt = await bcrypt.genSalt(12);
            password = await bcrypt.hash("kalbis123", salt);

            const create = await M_Admin_Divisi.create({
              username: nim,
              password,
              anggota_id: anggotaId.id,
              created_by: createdBy.nama,
            });

            delete anggota.dataValues[("id", "divisi_id")];
            delete create.dataValues["id"];
            const data = { ...anggota.dataValues, ...create.dataValues };
            res.status(200).send(data);
          }
        } else {
          const anggota = await M_Anggota.create({
            nama_depan,
            nama_belakang,
            nim,
            jabatan,
            angkatan,
            divisi_id,
            foto: "/images/" + req.file.filename,
            created_by:
              divisi.anggotum.nama_depan + " " + divisi.anggotum.nama_belakang,
          });

          if (anggota.divisi_id === 5) {
            delete anggota.dataValues[("id", "divisi_id")];
            res.status(200).send(anggota);
          } else {
            const createdBy = await M_Admin_Divisi.findOne({
              where: { username: req.user.username },
            });
            const anggotaId = await M_Anggota.findOne({ where: { nim } });
            const salt = await bcrypt.genSalt(12);
            const password = await bcrypt.hash("kalbis123", salt);

            const create = await M_Admin_Divisi.create({
              username: nim,
              password,
              anggota_id: anggotaId.id,
              created_by: createdBy.nama_depan + " " + createdBy.nama_belakang,
            });

            delete anggota.dataValues[("id", "divisi_id")];

            delete create.dataValues["id"];
            const data = { ...anggota.dataValues, ...create.dataValues };
            res.status(200).send(data);
          }
        }
      }
    } catch (error) {
      console.error(error.message);
      res.status(500).send("Server Error");
    }
  }
);

// GET All Data Anggota
router.get("/", async (req, res) => {
  try {
    const anggota = await M_Anggota.findAll({
      include: [
        {
          model: M_Divisi,
          required: true,
          attributes: { exclude: ["id"] },
        },
      ],
      attributes: { exclude: ["id", "divisi_id"] },
    });
    res.json(anggota);
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

// GET Single Data Anggota
router.get("/:nim", async (req, res) => {
  try {
    const check = await M_Anggota.findOne({
      where: { nim: req.params.nim },
    });
    if (!check) {
      res
        .status(400)
        .send({ errors: { msg: "There is no anggota with that nim" } });
    } else {
      const anggota = await M_Anggota.findOne({
        where: { nim: req.params.nim },
        include: [
          {
            model: M_Divisi,
            required: true,
            attributes: { exclude: ["id"] },
          },
        ],
        attributes: { exclude: ["id", "divisi_id"] },
      });
      res.json(anggota);
    }
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

// Delete Single Data
router.delete("/:nim", auth, async (req, res) => {
  try {
    const check = await M_Anggota.findOne({
      where: { nim: req.params.nim },
    });
    if (!check) {
      res.status(400).send({
        errors: { msg: "There is no Anggota with that nim" },
      });
    } else {
      const anggota = await M_Anggota.destroy({
        where: { nim: req.params.nim },
      });

      const adminDivisi = await M_Admin_Divisi.destroy({
        where: { username: req.params.nim },
      });
      res.status(200).send("Anggota Successfully Deleted");
    }
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

// Update Data Anggota
router.put(
  "/:nim",
  [
    check("nama_depan", "Field nama depan wajib di isi").not().isEmpty(),
    check("nama_belakang", "Field nama belakang wajib di isi").not().isEmpty(),
    check("nim", "Field  NIM wajib di isi").not().isEmpty(),
    check("jabatan", "Field jabatan wajib di isi").not().isEmpty(),
    check("angkatan")
      .not()
      .isEmpty()
      .withMessage("Field angkatan wajib di isi")
      .isLength({ min: 4, max: 4 })
      .withMessage("Field angkatan harus 4 angka")
      .isNumeric()
      .withMessage("Field angkatan harus berupa angka"),
    check("divisi_id", "Field divisi wajib di isi").not().isEmpty(),
  ],
  auth,
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      nama_depan,
      nama_belakang,
      nim,
      jabatan,
      angkatan,
      divisi_id,
    } = req.body;
    try {
      const check = await M_Anggota.findOne({
        where: { nim: req.params.nim },
      });
      if (!check) {
        res.status(400).send({
          errors: { msg: "There is no Anggota with that NIM" },
        });
      } else {
        const isAvailable = await M_Anggota.findOne({
          where: {
            [Op.and]: [
              {
                nim,
              },
              { nim: { [Op.not]: check.nim } },
            ],
          },
        });
        if (isAvailable) {
          res.status(400).json({
            errors: [
              {
                msg: "NIM already exists",
              },
            ],
          });
        } else {
          const admin = await M_Administrator.findOne({
            where: { username: req.user.username },
            attributes: { exclude: ["id"] },
          });

          const divisi = await M_Admin_Divisi.findOne({
            where: { username: req.user.username },
            attributes: { exclude: ["id", "anggota_id"] },
            include: [
              {
                model: M_Anggota,
                required: true,
                attributes: { exclude: ["id", "divisi_id"] },
                include: [
                  {
                    model: M_Divisi,
                    required: true,
                    attributes: { exclude: ["id"] },
                  },
                ],
              },
            ],
          });

          if (admin) {
            const anggota = await M_Anggota.update(
              {
                nama_depan,
                nama_belakang,
                nim,
                jabatan,
                angkatan,
                divisi_id,
                updated_by: admin.username,
              },
              {
                where: { nim: req.params.nim },
              }
            );

            const divisiUpdate = await M_Admin_Divisi.update(
              {
                username: nim,
              },
              { where: { username: req.params.nim } }
            );

            res.status(200).send("Anggota Successfully Updated");
          } else {
            const anggota = await M_Anggota.update(
              {
                nama_depan,
                nama_belakang,
                nim,
                jabatan,
                angkatan,
                divisi_id,
                updated_by:
                  divisi.anggotum.nama_depan +
                  " " +
                  divisi.anggotum.nama_belakang,
              },
              {
                where: { nim: req.params.nim },
              }
            );

            const divisiUpdate = await M_Admin_Divisi.update(
              {
                username: nim,
              },
              { where: { username: req.params.nim } }
            );

            res.status(200).send("Anggota Successfully Updated");
          }
        }
      }
    } catch (error) {
      console.error(error);
      res.status(500).send("Server Error");
    }
  }
);

module.exports = router;
