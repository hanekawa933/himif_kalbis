const express = require("express");
const router = express.Router();
const { check, validationResult } = require("express-validator");
const auth = require("../../middleware/auth");

const M_Pesan = require("../../models/M_Pesan");

router.post(
  "/",
  [
    check("nama", "Untuk mengirim pesan, field nama harus di isi")
      .not()
      .isEmpty(),
    check("email")
      .not()
      .isEmpty()
      .withMessage("Untuk mengirim pesan, field email harus di isi")
      .isEmail()
      .withMessage("Untuk mengirim pesan, email harus valid"),
    check("pesan", "Untuk mengirim pesan, field pesan harus di isi")
      .not()
      .isEmpty(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { nama, email, pesan } = req.body;
    try {
      const data = await M_Pesan.create({
        nama,
        email,
        pesan,
      });

      delete data.dataValues["id"];
      res.status(200).send(data);
    } catch (error) {
      console.error(error.message);
      res.status(500).send("Server Error");
    }
  }
);

router.get("/", auth, async (req, res) => {
  try {
    const data = await M_Pesan.findAll({ attributes: { exclude: ["id"] } });
    res.status(200).send(data);
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

module.exports = router;
