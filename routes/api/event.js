const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");

const { check, validationResult } = require("express-validator");

const multer = require("multer");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./public/images");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "_" + file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

var upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 10,
  },
  fileFilter: fileFilter,
});

const { M_Administrator, M_Admin_Divisi } = require("../../models/M_SuperUser");

const { M_Anggota, M_Divisi } = require("../../models/M_Anggota");

const {
  M_Event,
  M_Pengunjung,
  M_Tiket,
  M_Tamu,
} = require("../../models/M_Event");

router.post(
  "/",
  upload.single("poster"),
  [
    check("nama", "Field nama event wajib di isi").not().isEmpty(),
    check("description", "Field deskripsi event wajib di isi").not().isEmpty(),
    check("harga", "Field harga wajib di isi")
      .not()
      .isEmpty()
      .isNumeric()
      .withMessage("Field harga harus berupa angka"),
    check("rules", "Field rules event wajib di isi").not().isEmpty(),
    check("lokasi", "Field lokasi event wajib di isi").not().isEmpty(),
    check("tanggal", "Field tanggal event wajib di isi").not().isEmpty(),
    check("jam_mulai", "Field jam mulai event wajib di isi").not().isEmpty(),
    check("jam_berakhir", "Field jam berakhir event wajib di isi")
      .not()
      .isEmpty(),
    check("nama_tamu", "Field nama tamu event wajib di isi").not().isEmpty(),
    check("bangku_tersedia", "Field bangku tersedia event wajib di isi")
      .not()
      .isEmpty(),
  ],
  auth,
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const {
      nama,
      description,
      harga,
      rules,
      lokasi,
      tanggal,
      jam_mulai,
      jam_berakhir,
      bangku_tersedia,
      nama_tamu,
    } = req.body;
    try {
      const admin = await M_Administrator.findOne({
        where: { username: req.user.username },
        attributes: { exclude: ["id"] },
      });

      const divisi = await M_Admin_Divisi.findOne({
        where: { username: req.user.username },
        attributes: { exclude: ["id", "anggota_id"] },
        include: [
          {
            model: M_Anggota,
            required: true,
            attributes: { exclude: ["id", "divisi_id"] },
            include: [
              {
                model: M_Divisi,
                required: true,
                attributes: { exclude: ["id"] },
              },
            ],
          },
        ],
      });

      if (admin) {
        const event = await M_Event.create({
          nama,
          description,
          harga,
          rules,
          poster: "/images/" + req.file.filename,
          lokasi,
          tanggal,
          jam_mulai,
          jam_berakhir,
          bangku_tersedia,
          created_by: admin.nama,
        });

        const eventId = await M_Event.findOne({
          order: [["createdAt", "DESC"]],
        });

        const tamu = await M_Tamu.create({
          nama_tamu,
          event_id: eventId.id,
        });

        delete event.dataValues["id"];
        delete tamu.dataValues["id"];
        delete tamu.dataValues["event_id"];
        const data = { ...event.dataValues, ...tamu.dataValues };
        res.status(200).send(data);
      } else {
        const event = await M_Event.create({
          nama,
          description,
          harga,
          rules,
          poster: "/images/" + req.file.filename,
          lokasi,
          tanggal,
          jam_mulai,
          jam_berakhir,
          bangku_tersedia,
          created_by: divisi.nama_depan + " " + divisi.nama_belakang,
        });

        const eventId = await M_Event.findOne({
          order: [["createdAt", "DESC"]],
        });

        const tamu = await M_Tamu.create({
          nama_tamu,
          event_id: eventId.id,
        });

        delete event.dataValues["id"];
        delete tamu.dataValues["id"];
        delete tamu.dataValues["event_id"];
        const data = { ...event.dataValues, ...tamu.dataValues };
        res.status(200).send(data);
      }
    } catch (error) {
      console.error(error.message);
      res.status(500).send("Server Error");
    }
  }
);

// Get All Event
router.get("/", async (req, res) => {
  try {
    const event = await M_Event.findAll({
      include: [
        {
          model: M_Tamu,
          required: true,
          attributes: { exclude: ["id", "event_id"] },
        },
      ],
      attributes: { exclude: ["id"] },
    });
    res.json(event);
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

// Get Single Event
router.get("/:nama", async (req, res) => {
  try {
    const check = await M_Event.findOne({
      where: { nama: req.params.nama },
    });
    if (!check) {
      res
        .status(400)
        .send({ errors: { msg: "There is no Event with that event name" } });
    } else {
      const event = await M_Event.findOne({
        where: { nama: req.params.nama },
        include: [
          {
            model: M_Tamu,
            required: true,
            attributes: { exclude: ["id", "event_id"] },
          },
        ],
        attributes: { exclude: ["id"] },
      });
      res.json(event);
    }
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

// Delete Event
router.delete("/:event_name", auth, async (req, res) => {
  try {
    const check = await M_Event.findOne({
      where: { nama: req.params.event_name },
    });
    if (!check) {
      res
        .status(400)
        .send({ errors: { msg: "There is no Event with that name" } });
    } else {
      const tamu = await M_Tamu.destroy({
        where: { event_id: check.id },
      });

      const event = await M_Event.destroy({
        where: { nama: req.params.event_name },
      });
      res.status(200).send("Event Successfully Deleted");
    }
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

// Update Data Event
router.put(
  "/:nama",
  [
    check("nama", "Field nama event wajib di isi").not().isEmpty(),
    check("description", "Field deskripsi event wajib di isi").not().isEmpty(),
    check("harga", "Field harga wajib di isi")
      .not()
      .isEmpty()
      .isNumeric()
      .withMessage("Field harga harus berupa angka"),
    check("rules", "Field rules event wajib di isi").not().isEmpty(),
    check("lokasi", "Field lokasi event wajib di isi").not().isEmpty(),
    check("tanggal", "Field tanggal event wajib di isi").not().isEmpty(),
    check("jam_mulai", "Field jam mulai event wajib di isi").not().isEmpty(),
    check("jam_berakhir", "Field jam berakhir event wajib di isi")
      .not()
      .isEmpty(),
    check("nama_tamu", "Field nama tamu event wajib di isi").not().isEmpty(),
    check("bangku_tersedia", "Field bangku tersedia wajib di isi")
      .not()
      .isEmpty(),
  ],
  auth,
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      nama,
      description,
      harga,
      rules,
      poster,
      lokasi,
      tanggal,
      jam_mulai,
      jam_berakhir,
      bangku_tersedia,
    } = req.body;
    try {
      const check = await M_Event.findOne({
        where: { nama: req.params.nama },
      });
      if (!check) {
        res
          .status(400)
          .send({ errors: { msg: "There is no Event with that event name" } });
      } else {
        const admin = await M_Administrator.findOne({
          where: { username: req.user.username },
          attributes: { exclude: ["id"] },
        });

        const divisi = await M_Admin_Divisi.findOne({
          where: { username: req.user.username },
          attributes: { exclude: ["id", "anggota_id"] },
          include: [
            {
              model: M_Anggota,
              required: true,
              attributes: { exclude: ["id", "divisi_id"] },
              include: [
                {
                  model: M_Divisi,
                  required: true,
                  attributes: { exclude: ["id"] },
                },
              ],
            },
          ],
        });

        if (admin) {
          const event = await M_Event.update(
            {
              nama,
              description,
              harga,
              rules,
              lokasi,
              tanggal,
              jam_mulai,
              jam_berakhir,
              bangku_tersedia,
              updated_by: admin.nama,
            },
            { where: { nama: req.params.nama } }
          );
          res.status(200).send("Event Successfully Updated");
        } else {
          const event = await M_Event.update(
            {
              nama,
              description,
              harga,
              rules,
              poster,
              lokasi,
              tanggal,
              jam_mulai,
              jam_berakhir,
              bangku_tersedia,
              updated_by: divisi.nama_depan + " " + divisi.nama_belakang,
            },
            { where: { nama: req.params.nama } }
          );
          res.status(200).send("Event Successfully Updated");
        }
      }
    } catch (error) {
      console.error(error.message);
      res.status(500).send("Server Error");
    }
  }
);

module.exports = router;
