const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");

const { check, validationResult } = require("express-validator");

const multer = require("multer");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./public/files");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "_" + file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype ===
      "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
    file.mimetype ===
      "application/vnd.openxmlformats-officedocument.presentationml.presentation" ||
    file.mimetype === "application/pdf"
  ) {
    cb(null, true);
  } else {
    cb("Something went wrong", false);
  }
};

var upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 10,
  },
  fileFilter: fileFilter,
});

const { M_Administrator, M_Admin_Divisi } = require("../../models/M_SuperUser");
const { M_Anggota, M_Divisi } = require("../../models/M_Anggota");
const M_Dokumen = require("../../models/M_Dokumen");

router.post(
  "/",
  upload.single("file"),
  [
    check("nama", "Field nama dokumen wajib di isi").not().isEmpty(),
    check("description", "Field deskripsi dokumen wajib di isi")
      .not()
      .isEmpty(),
  ],
  auth,
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { nama, description, tipe_file, jenis_file } = req.body;
    try {
      const admin = await M_Administrator.findOne({
        where: { username: req.user.username },
        attributes: { exclude: ["id"] },
      });

      const divisi = await M_Admin_Divisi.findOne({
        where: { username: req.user.username },
        attributes: { exclude: ["id", "anggota_id"] },
        include: [
          {
            model: M_Anggota,
            required: true,
            attributes: { exclude: ["id", "divisi_id"] },
            include: [
              {
                model: M_Divisi,
                required: true,
                attributes: { exclude: ["id"] },
              },
            ],
          },
        ],
      });
      if (admin) {
        const dokumen = await M_Dokumen.create({
          nama,
          description,
          jenis_file,
          tipe_file,
          created_by: admin.nama,
          file: "/files/" + req.file.filename,
        });
        console.log(req.file);
        delete dokumen.dataValues["id"];
        res.status(200).send(dokumen);
      } else {
        const dokumen = await M_Dokumen.create({
          nama,
          description,
          jenis_file,
          tipe_file,
          created_by: divisi.nama_depan + " " + divisi.nama_belakang,
          file: "/files/" + req.file.filename,
        });
        console.log(req.file);
        delete dokumen.dataValues["id"];
        res.status(200).send(dokumen);
      }
    } catch (error) {
      console.error(error.message);
      res.status(500).send("Server Error");
    }
  }
);

router.put(
  "/:nama_dokumen",
  [
    check("nama", "Field nama dokumen wajib di isi").not().isEmpty(),
    check("description", "Field deskripsi dokumen wajib di isi")
      .not()
      .isEmpty(),
  ],
  auth,
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { nama, description, tipe_file, jenis_file, file } = req.body;
    try {
      const check = await M_Dokumen.findOne({
        where: { nama: req.params.nama_dokumen },
      });
      if (!check) {
        res.status(400).send({
          errors: { msg: "There is no Dokumen with that nama dokumen" },
        });
      } else {
        const admin = await M_Administrator.findOne({
          where: { username: req.user.username },
          attributes: { exclude: ["id"] },
        });

        const divisi = await M_Admin_Divisi.findOne({
          where: { username: req.user.username },
          attributes: { exclude: ["id", "anggota_id"] },
          include: [
            {
              model: M_Anggota,
              required: true,
              attributes: { exclude: ["id", "divisi_id"] },
              include: [
                {
                  model: M_Divisi,
                  required: true,
                  attributes: { exclude: ["id"] },
                },
              ],
            },
          ],
        });
        if (admin) {
          const dokumen = await M_Dokumen.update(
            {
              nama,
              description,
              jenis_file,
              tipe_file,
              file,
              updated_by: admin.nama,
            },
            {
              where: { nama: req.params.nama_dokumen },
            }
          );
          res.status(200).send("Dokumen Successfully Updated");
        } else {
          const dokumen = await M_Dokumen.update(
            {
              nama,
              description,
              jenis_file,
              tipe_file,
              file,
              updated_by: divisi.nama_depan + " " + divisi.nama_belakang,
            },
            {
              where: { nama: req.params.nama_dokumen },
            }
          );
          res.status(200).send("Dokumen Successfully Updated");
        }
      }
    } catch (error) {
      console.error(error.message);
      res.status(500).send("Server Error");
    }
  }
);

router.delete("/:nama", auth, async (req, res) => {
  try {
    const check = await M_Dokumen.findOne({
      where: { nama: req.params.nama },
    });
    if (!check) {
      res
        .status(400)
        .send({ errors: { msg: "There is no Dokumen with that name" } });
    } else {
      const dokumen = await M_Dokumen.destroy({
        where: { nama: req.params.nama },
      });
      res.status(200).send("Dokumen Successfully Deleted");
    }
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

router.get("/", async (req, res) => {
  try {
    const dokumen = await M_Dokumen.findAll({
      attributes: { exclude: ["id"] },
    });
    res.json(dokumen);
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

router.get("/:nama_dokumen", async (req, res) => {
  try {
    const dokumen = await M_Dokumen.findOne({
      where: { nama: req.params.nama_dokumen },
      attributes: {
        exclude: ["id"],
      },
    });
    if (!dokumen) {
      res.status(400).send({
        errors: { msg: "There is no Dokumen with that nama dokumen" },
      });
    } else {
      res.json(dokumen);
    }
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

module.exports = router;
